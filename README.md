# Covid-App

Die Covid-App ist eine ganzheitliche Lösung um in der Zeit des Corona-Viruses Besuche bei Restaurants zu dokumentieren, zu vergleichen, und Besucher zu benachrichtigen, falls ein Corona-Fall in ihrer Nähe aufgetreten ist.

## Vorgehen um Features anzuschauen (Tutorial)
1) normalen UserAccount erstellen
2) über generischen user "admin" einloggen (siehe nächster Absatz)
3) Im Schritt 1) erstellten user auf admin upgraden ("Elevate User")
4) Erneut mit dem in Schritt 1) erstellten user einloogen
5) Beispieldateien einlesen
6) neuen User erstellen
7) Jetzt kann man sämtliche Features des Users benutzen
8) Einloggen als Manager
9) Mit Doppelklick auf einen Restaurantbesuch kann man sich zeitgleiche Besuche anzeigen lassen

## Usage
Die erste Page ist **Login**. Von hier aus kann das Programm wie normal benutzen. 
Des Testens wegen gibt es folgende Logins die auf die jeweilige Seite hinweisen:

| Username | Passwort |
| ------------- | ------------- |
| admin  | admin |
| manager  | manager  |
| besitzer| besitzer|
| genericuser| genericuser|

Bitte beachten, dass **besitzer** und **genericuser** keine eigenen DB-Einträge haben, sodass manche Use-Cases nicht funktionieren werden.

### Login 
**Passwort vergessen** 

Die Email wird direkt von einer Gmail Addresse ausgesendet. In der App.config ist eine weitere *smtp deliveryMethod*, die zum Testen verwendet werden kann. Falls keine E-Mail ankommt, muss man sich mit dem Gerät im Browser bei Google mit den passenden Daten anmelden.

### Admin
**Elevate User**

Nach einer erfolgreichen **Registration** ist ein User Account angelegt. Dieser kann im Admin-Menü andere Berechtigungen zugewiesen bekommen. Zuerst entweder E-Mail oder UserID eingeben und *Suche User* anklicken. Damit ist ein User ausgewählt. Dann in der Combobox ein passendes Berechtigungslevel auswählen und mit *Set Level* bestätigen.

**Datensätze anlegen** 

Entweder den Pfad direkt eingeben oder mit Doppelklick den Filebrowser aufrufen. Es ist wichtig, dass die .csv korrekt formatiert sind. **Besitzer** müssen zuerst eingelesen werden, da Restaurants direkt Besitzern zugeordnet werden.

-> Siehe beigelegte Beispiel-Dateien: *Restaurantbesitzer.csv* & *Restaurant.csv*

**Download Besucher**

Einen schreib-berechtigten Ordner auswählen und mit *Download* bestätigen. Es werden alle Besucher in einer .xml Datei abgespeichert. 



### Restaurantbesitzer
**Besuche einlesen**
In der ComboBox ist eine Liste aller Restaurants die ein Besitzer hat. Direkt einen Dateipfad eingeben oder mit Doppelklick auswählen. Die Datei muss korrekt formatiert sein.

-> Siehe beigelegte Beispiel-Datei: *Besuche.txt*


## Use Cases
### Nutzer

**Gäste**
- Die normalen Nutzer der Applikation. 
- Sie können sich registrieren und sich mit ihrer Emailaddresse und Passwort einloggen.
- Falls sie das Passwort vergessen haben, können sie sich ein neues Passwort zuschicken lassen.
- Im Hauptmenü kann das Passwort geändert werden.
- Im Hauptmenü können sie neue Besuche anlegen, indem sie ein Restaurant auswählen und eine Zeitraum eingeben
- Sie können sich ihre eigenen Besuche anzeigen lassen.
- Sie können sich Covid-positiv melden, damit andere Restaurantsgäste benachrichtig werden können


**Restaurantbesitzer**
- Besitzer können selbstständig neue Restaurants anlegen
- Besitzer können eines ihrer Restaurants auswählen und eine (korrekt formattierte) Text-Datei an Besuchen einlesen - Falls Besucher sich nicht registrieren wollen.

**Covid-Manager**
- Der Covid-Manager ist vom Freistaat Bayern eingestellt um die Bürger zu überwachen
- Seine Aufgabe ist es die Besuch-Liste zu beobachten, dazu hat er mehrere Filter um die Daten aufzulisten
- Falls ein Besucher sich Covid-positiv meldet, kann er sich automatisch alle deren Besuche auflisten lassen und mit Knopfdruck eine Warn-E-Mail an alle senden, die sich zur selben Zeit in dem Restaurant aufhielten.
- Zusätzlich kann er, des Datenschutzes wegen, alle Besuche die älter als 14 Tage sind löschen

**Admin**
- Der Admin ist für den Setup am Anfang und die Berechtigungen der Useraccounts zuständig.
- Er kann eine Liste an Besitzern hochladen, die gleich angelegt werden.
- Er kann eine Liste an Restaurants hochladen, die den Besitzern zugeteilt werden.
- Er kann die E-Mail Addresse oder UserID eingeben um einen Benutzer auszuwählen und diesen dann zu **User**, **Besitzer** oder **Admin** zu machen
- Er kann Besucherdaten abspeichern um sie an Big Data zu verkaufen.

