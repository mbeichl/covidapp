﻿using CovidApp.BesuchVerwaltung;
using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Et.Impl;
using CovidApp.BesuchVerwaltung.Persistence;
using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidApp
{
    public class Applikation
    {
        public static readonly ILog log = LogManager.GetLogger(typeof(Applikation));
        static void Main(string[] args)
        {
            XmlConfigurator.Configure();
            log.Info("Start Logging\n");

            #region debugging

            /*
                        //Erzeuge Besuch Verwaltung Logik
                        IBesuchPersistence besuchPers = new BesuchPersistenceEF();
                        IRestaurantPersistence restaurantPers = new RestaurantPersistenceEF();
                        IBesucherPersistence besucherPers = new BesucherPersistenceEF();
                        IRestaurantBesitzerPersistence besitzerPers = new RestaurantBesitzerPersistenceEF();

                        BesuchVerwaltungFactory fac = new BesuchVerwaltungFactory(besuchPers, restaurantPers, besucherPers, besitzerPers);
                        IVerwaltungAdmin admin = fac.getBesuchAdmin();

            restaurantPers.DeleteAllRestaurants();
            besitzerPers.DeleteBesitzer("yoshimura@muenchen-mail.de");

            RestaurantBesitzer restaurantbesitzer1 = new RestaurantBesitzer("yoshimura@muenchen-mail.de", "Ligusterweg 3", "Yoshimura", 123456789);


                        Restaurant restaurant1 = new Restaurant("Antik", "Schillerweg 33", "Perla@gmail.com", 92392);

                        admin.AddRestaurantBesitzer(restaurantbesitzer1);
                        admin.AddRestaurantToBesitzer("yoshimura@muenchen-mail.de", restaurant1);

                        IBesucher Levi = new Besucher("Levi", "Kaneki", "levi@muenchen - mail.de");
                        IBesucher Erwin = new Besucher("Erwin", "Shiki", "erwin@muenchen - mail.de");
                        IBesucher Eren = new Besucher("Eren", "Chan", "eren@muenchen - mail.de");
                        besucherPers.CreateBesucher(Levi);
                        besucherPers.CreateBesucher(Erwin);
                        besucherPers.CreateBesucher(Eren);


                        IBesuch besuch5 = new Besuch((Besucher)Levi, (Restaurant)restaurant1, new DateTime(2020, 12, 01, 10, 00, 00), new DateTime(2020, 12, 01, 12, 00, 00)); //POSITIV, 10-12 uhr
                        IBesuch besuch6 = new Besuch((Besucher)Eren, (Restaurant)restaurant1, new DateTime(2020, 12, 20, 09, 00, 00), new DateTime(2020, 12, 23, 11, 00, 00));      //9-11Uhr
                        IBesuch besuch7 = new Besuch((Besucher)Levi, (Restaurant)restaurant1, new DateTime(2020, 01, 01, 11, 00, 00), new DateTime(2020, 01, 01, 13, 00, 00));     //11-13uhr
                        IBesuch besuch8 = new Besuch((Besucher)Erwin, (Restaurant)restaurant1, new DateTime(2021, 01, 01, 07, 00, 00), new DateTime(2021, 01, 01, 09, 00, 00)); //7-9 Uhr
                        IBesuch besuch9 = new Besuch((Besucher)Eren, (Restaurant)restaurant1, new DateTime(2021, 01, 03, 11, 00, 00), new DateTime(2021, 01, 03, 12, 00, 00)); //am 3.1

                        admin.AddBesuch(besuch5);
                        admin.AddBesuch(besuch6);
                        admin.AddBesuch(besuch7);
                        admin.AddBesuch(besuch8);
                        admin.AddBesuch(besuch9);
            */


            //Erzeuge Besuch Verwaltung Logik
            IBesuchPersistence besuchPers = new BesuchPersistenceEF();
            IRestaurantPersistence restaurantPers = new RestaurantPersistenceEF();
            IBesucherPersistence besucherPers = new BesucherPersistenceEF();
            IRestaurantBesitzerPersistence besitzerPers = new RestaurantBesitzerPersistenceEF();

            BesuchVerwaltungFactory fac = new BesuchVerwaltungFactory(besuchPers, restaurantPers, besucherPers, besitzerPers);

            IVerwaltungAdmin admin = fac.getBesuchAdmin();

            /*  restaurantPers.DeleteAllRestaurants();
              besitzerPers.DeleteBesitzer("yoshimura@muenchen-mail.de");
              besitzerPers.DeleteBesitzer("mbeichl@muenchen-mail.de");



              RestaurantBesitzer rb = new RestaurantBesitzer("mbeichl@muenchen-mail.de", "deine-muddi-straße", "kek", "1234556677");
              verwaltungAdmin.AddRestaurantBesitzer(rb);
              Restaurant r1 = new Restaurant("Antik", "deine-muddi-strasse", "deinemuddi@gmx.de", "33344456");
              verwaltungAdmin.AddRestaurantToBesitzer("mbeichl@muenchen-mail.de",r1);*/

            /* foreach (IBesuch b in verwaltungAdmin.ListBesuche())
             {
                 String name = verwaltungAdmin.DictionaryBesucher()[b.BesucherID].Vorname + " " + verwaltungAdmin.DictionaryBesucher()[b.BesucherID].Nachname;
                 Console.WriteLine(name);
                 String Zeitraum = b.anfang.ToString() + " - " + b.anfang.ToString();
                 Console.WriteLine(Zeitraum);

                 String Restaurant = verwaltungAdmin.DictionaryRestaurants()[b.RestaurantID].Name;
                 Console.WriteLine(Restaurant);

             }*/

            CovidApp.Logger.log = LogManager.GetLogger("APP_log");
            CovidApp.Logger.StartLogging();


           


            List<IBesuch> besuche = admin.BesucheZurGleichenZeit(3);
            foreach (IBesuch b in besuche)
            {
                Console.WriteLine(b.ToString());
            }




            /*  foreach(IBesuch b in verwaltungAdmin.ListBesuche())
              {
                  Console.WriteLine(b.BesucherID + " "+ b.RestaurantID);
              }

              Console.WriteLine(admin.DictionaryRestaurants()[b.Restaurant.RestaurantID].Name);
             */
            // Console.WriteLine(verwaltungAdmin.DictionaryBesucher()[2].Vorname);


            #endregion debugging 


        }

        }
    public class Logger
    {
        public static ILog log;

        public static void StartLogging()
        {
            XmlConfigurator.Configure();
            log.Info("Start Logging --- Logger");
        }
    }
}
