﻿using CovidApp.AccountVerwaltung.Persistence;
using CovidApp.AccountVerwaltung.Util;
using CovidApp.AccountVerwaltung.Util.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CovidApp.AccountVerwaltung.Logic.Impl
{
    public class AccountAdmin : IAccountAdmin
    {
        
        private IDictionary<int, UserAccount> accounts = new SortedList<int, UserAccount>();

        private IAccountPersistence persistence;

        public AccountAdmin(IAccountPersistence persistence)
        {
            this.persistence = persistence;
            accounts = persistence.LoadAccounts();
        }

        public void ChangePassword(int ID, string password)
        {
            if (accounts.ContainsKey(ID))
            {
                UserAccount u = accounts[ID];
                u.ChangePassword(password);
                persistence.UpdatePassword(u.userID, password);
                accounts = persistence.LoadAccounts();
                Logger.log.Info("Passwort für User: " + ID + " geändert auf neu: " + password);
            }            
        }

        public void ChangeUserAccount(int ID, string email, string vorname, string nachname, int level)
        {
            if (accounts.ContainsKey(ID))
            {
                UserAccount u = accounts[ID];
                u.UpdateUserAccount(email, vorname, nachname);
                persistence.UpdateAccount(u.userID, email, vorname, nachname, level);
                accounts = persistence.LoadAccounts();
            }
        }
        public void Refresh()
        {
            this.accounts = persistence.LoadAccounts();
        }

        public UserAccount CreateUserAccount(string password, string email, string vorname, string nachname)
        {
            UserAccount userAccount = new UserAccount();
            
            var validateEmail = new Func<string, bool>(b => {
                try
                {
                    return Regex.IsMatch(email,
                        @"^[^@\s]+@[^@\s]+\.[^@\s]+$",
                        RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
                }
                catch (RegexMatchTimeoutException)
                {
                    return false;
                }
            });            
            
            if (String.IsNullOrEmpty(vorname) || vorname == "Vorname") throw new Exception("Vorname ungültig");
            else if (String.IsNullOrEmpty(nachname) || nachname == "Nachname") throw new Exception("Nachname ungültig");

            else if(String.IsNullOrEmpty(password) || password == "Passwort") throw new Exception("Passwort ungültig");
            else if(password.Length<4) throw new Exception("Passwort zu kurz");

            else if(String.IsNullOrEmpty(email)) throw new Exception("E-Mail ungültig");
            else if(!validateEmail(email)) throw new Exception("E-Mail Format ungültig");

            else if(String.IsNullOrEmpty(vorname)) throw new Exception("Vorname ungültig");
            else if(CheckDuplicate(email)) throw new Exception("E-Mail Addresse bereits verwendet");

            else 
            {
                userAccount = new UserAccount(password, email, vorname, nachname);
                persistence.CreateAccount(userAccount);
                accounts = persistence.LoadAccounts();
                Logger.log.Info("Account angelegt (Email: " + email + ")");
            }
            return userAccount; 
        }

        public void AddBesucherID(int UserID, int BesucherID)
        {
            if (!accounts.ContainsKey(UserID))
            {
                Logger.log.Info("UserID nicht gefunden");
                throw new Exception("UserID nicht gefunden");
            }
            else if (BesucherID == 0)
            {
                Logger.log.Info("BesucherID nicht gefunden");
                throw new Exception("BesucherID nicht gefunden");
            }
            else
            {
                persistence.UpdateID(UserID, BesucherID);
                accounts = persistence.LoadAccounts();
                Logger.log.Info("BesucherID [" + BesucherID + "] hinzugefügt");
            }
        }
        public void AddBesitzerID(int UserID, string BesitzerID)
        {
            if (!accounts.ContainsKey(UserID))
            {
                Logger.log.Info("UserID nicht gefunden");
                throw new Exception("UserID nicht gefunden");
            }
            else if (BesitzerID == "")
            {
                Logger.log.Info("BesitzerID nicht gefunden");
                throw new Exception("BesitzerID nicht gefunden");
            }
            else
            {
                persistence.UpdateBesitzerID(UserID, BesitzerID);
                accounts = persistence.LoadAccounts();
                Logger.log.Info("BesitzerID [" + BesitzerID + "] hinzugefügt");
            }
        }

        public void DeleteUserAccount(int ID)
        {
            if (accounts.ContainsKey(ID))
            {
                accounts.Remove(ID);
                persistence.DeleteAccount(ID);
                accounts = persistence.LoadAccounts();
                Logger.log.Info("Account mit ID [" +ID +"] gelöscht");
            }
        }

        public string GetRandomPassword()
        {
            int length = 8;
            const string allowedCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder pw = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                pw.Append(allowedCharacters[rnd.Next(allowedCharacters.Length)]);
            }
            Logger.log.Info("Random Passwort generiert: " + pw);
            return pw.ToString();
        }

        public bool CheckDuplicate(String Email)
        {
            //TODO: IQueryable impl?
            Logger.log.Info("E-Mail Addresse: [" + Email + "] wird auf Duplikat geprüft");

            foreach (int i in accounts.Keys)
            {

                if (String.Equals(accounts[i].email,Email, StringComparison.OrdinalIgnoreCase))
                {
                    Logger.log.Info("[" + accounts[i] + "] " + "+ [" + accounts[i].email + "] ist ein Duplikat");
                    return true;
                }
            }
            Logger.log.Info("Email Adresse: [" + Email + "] ist nicht in der Datenbank");
            return false;
        }

        
    }
}
