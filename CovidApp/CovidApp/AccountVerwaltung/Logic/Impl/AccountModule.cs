﻿using CovidApp.AccountVerwaltung.Persistence;
using CovidApp.AccountVerwaltung.Util.Impl;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CovidApp.Applikation;

namespace CovidApp.AccountVerwaltung.Logic.Impl
{
    public class AccountModule : IAccountModule
    {

        IAccountPersistence persistence;
        IDictionary<int, UserAccount> accounts = new SortedList<int, UserAccount>();

        public AccountModule(IAccountPersistence persistence)
        {
            this.persistence = persistence;
            accounts = persistence.LoadAccounts();
        }

        //TODO: AccountModule Impl

   
        public int[] validieren(string Email, string pw)
        {
            accounts = persistence.LoadAccounts();
            int key = accounts.FirstOrDefault(x => x.Value.email == Email).Key;
            Logger.log.Info("Validieren Key lookup ergibt: " + key);

            if (key != 0 && accounts[key].password == pw)
            {
                int[] res = new int[] { accounts[key].BesucherID, accounts[key].level };
                return res;
            }
            else return new int[] { 0, 0 };
        }    
        public int getUserID(string Email)
        {
            accounts = persistence.LoadAccounts();
            int ID = accounts.FirstOrDefault(x => x.Value.email == Email).Key;
            Logger.log.Info("getUserID Key lookup ergibt: " + ID);
            return ID;
        }
        public string getBesitzerID(string Email)
        {
            accounts = persistence.LoadAccounts();
            int key = accounts.FirstOrDefault(x => x.Value.email == Email).Key;
            string besID = accounts[key].RestaurantBesitzerID; 
            Logger.log.Info("Besitzer Key lookup ergibt: " + besID);

            return besID;
        }
    }
}
