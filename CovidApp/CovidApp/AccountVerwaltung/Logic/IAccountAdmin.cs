﻿using CovidApp.AccountVerwaltung.Util.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidApp.AccountVerwaltung.Logic
{
    public interface IAccountAdmin
    {
        /// <summary>
        /// UserAccount wird gelöscht
        /// </summary>  
        void DeleteUserAccount(int ID);

        /// <summary>
        /// UserAccount wird erstellt
        /// </summary>
        UserAccount CreateUserAccount(string password, string email, string vorname, string nachname);

        /// <summary>
        /// UserAccount wird geändert
        /// </summary>
        /// <param name="ID">Betroffener UserAccount</param>
        /// <param name="email"></param>
        /// <param name="vorname"></param>
        /// <param name="nachname"></param>
        void ChangeUserAccount(int ID, String email, String vorname, String nachname, int level);

        /// <summary>
        /// Änderung des Passwortes des UserAccounts
        /// </summary>
        /// <param name="ID">Betroffener UserAccount</param>
        /// <param name="password">Neues Passwort</param>
        void ChangePassword(int ID, string password);

        /// <summary>
        /// Fügt dem Account einen Besucher reference key hinzu
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="BesucherID"></param>
        void AddBesucherID(int UserID, int BesucherID);

        /// <summary>
        /// Fügt dem Account einen Besitzer reference key hinzu
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="BesitzerID"></param>
        void AddBesitzerID(int UserID, string BesitzerID);

        /// <summary>
        /// Erstellt ein zufälliges passwort
        /// </summary>
        String GetRandomPassword();

        /// <summary>
        /// Stellt fest ob eine Email-Addresse schon abgespeichert ist
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        bool CheckDuplicate(String email);

        /// <summary>
        /// Lädt die Persistenten Daten neu
        /// </summary>
        void Refresh();


    }
}
