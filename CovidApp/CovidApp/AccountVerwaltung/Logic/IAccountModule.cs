﻿using CovidApp.AccountVerwaltung.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidApp.AccountVerwaltung.Logic
{
    public interface IAccountModule
    {
        /// <summary>
        /// Checkt ob eine Email in der Liste von Accounts enthälten ist und gibt die passende UserID zurück.
        /// Wenn nein, gibt 0 zurück
        /// </summary>
        /// <param name="Email"></param>
        /// <param name="Passwort"></param>
        /// <returns>Int Array mit UserID und Level</returns>
        int[] validieren(string Email, string Passwort);

        /// <summary>
        /// Sucht eine UserID nach Email-Addresse
        /// </summary>
        /// <param name="Email"></param>
        /// <returns>UserID</returns>
        int getUserID(string Email);
        /// <summary>
        /// Sucht eine BesitzerID nach Email Addresse
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        string getBesitzerID(string Email);

    }
}
