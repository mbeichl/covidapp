﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using CovidApp.AccountVerwaltung.Util.Impl;
using CovidApp.AccountVerwaltung.Util;

namespace CovidApp.AccountVerwaltung.Persistence
{
    public class AccountPersistence : IAccountPersistence
    {

        public void CreateAccount(UserAccount userAccount)
        {
            using(AccountContext db = new AccountContext())
            {
                db.Accounts.Add(userAccount);
                db.SaveChanges();
            }
        }

        public void DeleteAccount(int id)
        {
            using(AccountContext db = new AccountContext())
            {
                UserAccount a = db.Accounts.Find(id);
                //falls UserAccount mit der entsprechenden id gefunden wurde
                //UserAccount löschen
                if (a != null)
                {
                    db.Accounts.Remove(a);
                    db.SaveChanges();
                }
            }
        }

        public IDictionary<int, UserAccount> LoadAccounts()
        {
            IDictionary<int, UserAccount> accounts = new SortedList<int, UserAccount>();

            using (AccountContext db = new AccountContext())
            {
                IEnumerable<UserAccount> users = db.Accounts.ToList();


                foreach (UserAccount u in users)
                {
                    accounts.Add(u.userID, u);
                }

            }
            return 
            accounts;
            
        }


        public void SaveUserAccounts(IDictionary<int, UserAccount> list)
        {
            using(AccountContext db = new AccountContext())
            {
                //sequenziell list durchlaufen
                foreach(KeyValuePair<int,UserAccount> a in list)
                {
                    //UserAccount bereits in DB?
                    UserAccount ua = db.Accounts.Find(a.Value.userID);

                    //Wenn nicht in DB vorhanden
                    if (ua == null)
                    {
                        //UserAccount hinzufügen
                        db.Accounts.Add(a.Value);
                    }
                }
                db.SaveChanges();
            }
        }

        public void UpdateAccount(int id, string email, string vorname, string nachname, int level)
        {
            using (AccountContext db = new AccountContext())
            {
                //Ist der zu ändernde UserAccount überhaupt in der DB?
                UserAccount ua = db.Accounts.Find(id);

                //Falls in DB enthalten -> ändern
                if (ua != null)
                {
                    if (!String.IsNullOrEmpty(email)) ua.email = email;
                    if (!String.IsNullOrEmpty(vorname)) ua.vorname = nachname;
                    if (!String.IsNullOrEmpty(nachname)) ua.nachname = nachname;
                    if (level <3 && level >=0) ua.level = level;


                    //ua.UpdateUserAccount(email, vorname, nachname);
                    db.SaveChanges();
                }
            }
        }
        public void UpdateID(int id, int BesucherID)
        {
            using (AccountContext db = new AccountContext())
            {
                UserAccount ua = db.Accounts.Find(id);
                if (ua != null)
                {
                    if (BesucherID > 0) ua.BesucherID = BesucherID;
                    else throw new Exception("BesucherID 0 oder nicht gefunden");
                    db.SaveChanges();
                    Logger.log.Info("Schreibe ID auf UserAccount");
                }
            }
        }

        public void UpdateBesitzerID(int id, string BesitzerID)
        {
            using (AccountContext db = new AccountContext())
            {
                UserAccount ua = db.Accounts.Find(id);
                if (ua != null)
                {
                    ua.RestaurantBesitzerID = BesitzerID;
                    db.SaveChanges();
                    Logger.log.Info("Schreibe BesitzerID auf UserAccount");
                }
            }
        }

        public void UpdatePassword(int id, string password)
        {
            using (AccountContext db = new AccountContext())
            {
                //Ist der zu ändernde UserAccount überhaupt in der DB?
                UserAccount ua = db.Accounts.Find(id);

                //Falls in DB enthalten -> passwort ändern
                if (ua != null)
                {
                    ua.ChangePassword(password);
                    db.SaveChanges();
                }
            }
        }
        public void UpdateAccountLvl(int id, int level)
        {
            using (AccountContext db = new AccountContext())
            {
                UserAccount ua = db.Accounts.Find(id);
                if (ua != null)
                {
                    if (level < 3 && level >= 0) ua.level = level;
                    db.SaveChanges();
                }
                else throw new Exception("Account nicht gefunden!");
            }
        }
    }
}
