﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using CovidApp.AccountVerwaltung.Util.Impl;

namespace CovidApp.AccountVerwaltung.Persistence
{
    public class AccountContext : DbContext
    {
        public AccountContext() : base("Account-Datenbank")
        {
            //Neue Datenbank wenn das Modell sich ändert
            Database.SetInitializer<AccountContext>(new DropCreateDatabaseIfModelChanges<AccountContext>());
        }
        public DbSet<UserAccount> Accounts { get; set; }
    }
}
