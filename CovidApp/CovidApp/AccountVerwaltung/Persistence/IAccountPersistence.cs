﻿using CovidApp.AccountVerwaltung.Util;
using CovidApp.AccountVerwaltung.Util.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidApp.AccountVerwaltung.Persistence
{
    public interface IAccountPersistence
    {
        /// <summary>
        /// Alle persistent gespeicherten UserAccounts laden
        /// </summary>
        /// <returns>UserAccountliste</returns>
        IDictionary<int, UserAccount> LoadAccounts();

        /// <summary>
        /// Erzeugt persistent einen neuen UserAccount
        /// </summary>
        /// <param name="userAccount"></param>
        void CreateAccount(UserAccount userAccount);

        /// <summary>
        /// Alle UserAccounts persistent speichern
        /// </summary>
        /// <param name="list"></param>
        void SaveUserAccounts(IDictionary<int, UserAccount> list);

        /// <summary>
        /// UserAccount persistent ändern
        /// </summary>
        /// <param name="id"></param>
        /// <param name="email"></param>
        /// <param name="vorname"></param>
        /// <param name="nachname"></param>
        void UpdateAccount(int id, string email, string vorname, string nachname, int level);

        /// <summary>
        ///  Ändert die BesucherID Referenz des UserAccounts
        /// </summary>
        /// <param name="id"></param>
        /// <param name="BesucherID"></param>
        void UpdateID(int id, int BesucherID);
        /// <summary>
        /// Ändert die BesitzerID Referenz des UserAccounts
        /// </summary>
        /// <param name="id"></param>
        /// <param name="BesitzerID"></param>
        void UpdateBesitzerID(int id, string BesitzerID);

        /// <summary>
        /// UserAccount passwort persistent ändern
        /// </summary>
        /// <param name="id"></param>
        /// <param name="password"></param>
        void UpdatePassword(int id, string password);

        /// <summary>
        /// UserAccount persistent löschen
        /// </summary>
        /// <param name="id"></param>
        void DeleteAccount(int id);
        /// <summary>
        /// Verändert das Berechtigungslevel des Accounts
        /// </summary>
        /// <param name="id"></param>
        /// <param name="level"></param>
        void UpdateAccountLvl(int id, int level);
    }
}
