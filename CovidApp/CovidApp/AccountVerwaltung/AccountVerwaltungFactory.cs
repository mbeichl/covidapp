﻿using CovidApp.AccountVerwaltung.Logic;
using CovidApp.AccountVerwaltung.Logic.Impl;
using CovidApp.AccountVerwaltung.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidApp.AccountVerwaltung
{
    public class AccountVerwaltungFactory
    {
        private AccountAdmin admin;
        private AccountModule module;

        public AccountVerwaltungFactory(IAccountPersistence persistence)
        {
            admin = new AccountAdmin(persistence);
            module = new AccountModule(persistence);
        }

        public IAccountAdmin getAccountAdmin()
        {
            return admin;
        }

        public IAccountModule getAccountModule()
        {
            return module;
        }
    }
}
