﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidApp.AccountVerwaltung.Util
{
    public interface IUserAccount
    {
        int userID { get; set; }
        string password { get; set; }
        string vorname { get; set; }
        string nachname { get; set; }
        int level { get; set; }
        int BesucherID { get; set; }
        string RestaurantBesitzerID { get; set; }
        string email { get; set; }


        /// <summary>
        /// Passwort ändern
        /// </summary>
        /// <param name="password"></param>
        void ChangePassword(string password);

        /// <summary>
        /// UserAccount updaten
        /// </summary>
        /// <param name="email"></param>
        /// <param name="vorname"></param>
        /// <param name="nachname"></param>
        void UpdateUserAccount(string email, string vorname, string nachname);


    }
}
