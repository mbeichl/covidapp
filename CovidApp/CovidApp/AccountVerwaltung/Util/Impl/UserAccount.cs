﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CovidApp.BesuchVerwaltung.Et;

namespace CovidApp.AccountVerwaltung.Util.Impl
{
    /// <summary>
    /// UserAccount Entity
    /// </summary>
    /// 
    [Serializable]
    public class UserAccount : IComparable<UserAccount>, IUserAccount
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int userID { get; set; }

        [MaxLength(30)]
        public string password { get; set; }
        [MaxLength(45)]
        public string email { get; set; }
        [MaxLength(30)]
        public string vorname { get; set; }
        [MaxLength(30)]
        public string nachname { get; set; }
        [Required]
        public int level { get; set; }
        public int BesucherID { get; set; }
        public virtual IBesucher besucher { get; set; }
        public string RestaurantBesitzerID { get; set; }

        public UserAccount() { }

        public UserAccount(string password, string email, string vorname, string nachname)
        {
            this.password = password;
            this.email = email;
            this.vorname = vorname;
            this.nachname = nachname;
            this.level = 0;
        }              
        
        public void ChangePassword(string password)
        {
            this.password = password;
        }

        public void UpdateUserAccount(string email, string vorname, string nachname)
        {
            this.email = email;
            this.vorname = vorname;
            this.nachname = nachname;
        }

        #region IComparable
        public int CompareTo(UserAccount other)
        {
            return userID.CompareTo(other.userID);
        }
        #endregion

        #region Equals, HashCode 
        public override bool Equals(object obj)
        {
            return obj is UserAccount account &&
                   userID == account.userID &&
                   password == account.password &&
                   email == account.email &&
                   vorname == account.vorname &&
                   nachname == account.nachname;
        }

        public override int GetHashCode()
        {
            int hashCode = -599579039;
            hashCode = hashCode * -1521134295 + userID.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(password);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(email);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(vorname);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(nachname);
            return hashCode;
        }

        public override string ToString()
        {
            return vorname + " "+nachname;
        }
        #endregion
    }
}
