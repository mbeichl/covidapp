﻿using CovidApp.AccountVerwaltung.Util;
using CovidApp.BesuchVerwaltung.Et.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidApp.AccountVerwaltung
{
    /// <summary>
    /// Extra Komponente, die nur für den Admin zugänglich ist
    /// </summary>
    public interface IAdminHelper
    {
        /// <summary>
        /// Liest eine csv Datei mit Restaurants und sortiert diese nach Restaurantbesitzern
        /// </summary>
        /// <param name="str">Path</param>
        /// <returns></returns>
        SortedDictionary<Restaurant, string> ReadRestaurantsFromFile(string str);
        /// <summary>
        /// Liest eine csv Datei mit Restaurantbesitzern
        /// </summary>
        /// <param name="str">Path</param>
        /// <returns></returns>
        List<RestaurantBesitzer> ReadRestaurantBesitzerFromFile(string str);

        /// <summary>
        /// Sucht einen Account nach eingegebener Email Addresse
        /// </summary>
        /// <param name="input"></param>
        /// <returns>IUserAccount</returns>
        IUserAccount getAccount(String input);
        /// <summary>
        /// Sucht einen Account nach ID
        /// </summary>
        /// <param name="input"></param>
        /// <returns>IUserAccount</returns>
        IUserAccount getAccount(int input);
        /// <summary>
        /// Setzt das Berechtigungslevel eines Accounts
        /// </summary>
        /// <param name="IDinput"></param>
        /// <param name="Level"></param>
        void elevateAccount(int IDinput, int Level);

        /// <summary>
        /// Speichert Besucher Daten in .xml Datei ab
        /// </summary>
        void downloadDB(string path);

    }
}
