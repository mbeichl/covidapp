﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Et.Impl;

namespace CovidApp.BesuchVerwaltung
{
    public interface IVerwaltungAdmin
    {
        //Besuch
        void AddBesuch(IBesucher besucher, Restaurant Restaurant, DateTime? Anfang, DateTime? Ende);
        void DeleteBesuch(int id);
        List<IBesuch> ListBesuche();

        IBesuch BesuchAnlegen(IBesucher besucher, IRestaurant restaurant, DateTime datumVon, DateTime datumBis);

        IDictionary<int, IBesuch> DictionaryBesuche();


        //Besucher
        IBesucher AddBesucher(string vorname, string nachname, string email);
        void AddBesuchToBesucher(int besucherID, Besuch b);
        void DeleteBesucher(int id);
        List<IBesucher> ListBesucher();
        IDictionary<int, IBesucher> DictionaryBesucher();

        //Restaurant
        void AddRestaurant(Restaurant r);
        void DeleteRestaurant(int id);
        IList<Restaurant> ListRestaurants();
        IDictionary<int, IRestaurant> DictionaryRestaurants();
        void AddBesuchToRestaurant(int resID, Besuch b);


        //Besitzer
        IRestaurantBesitzer AddRestaurantBesitzer(RestaurantBesitzer rb);
        void DeleteRestaurantBesitzer(string id);
        IList<RestaurantBesitzer> ListRestaurantBesitzer();
        void AddRestaurantToBesitzer(string besitzer, Restaurant r);


        //meldet Besucher mit id als covid positiv, PositveTest Date gibt das Datum des positven Testes an
        //updated covid Status und gibt falshe zurück falls user mit der ID nicht existiert
        bool meldePositiv(int id, DateTime? PositveTestDate);

        //geht Alle Besuche durch: Wenn: Besuchszeitraum überschneidet mit Covid-Positvem-Besuch: dann:Besucher dieses Besuches auch positiv melden
        void AlleBesuchDatenVergleichen();
        List<IBesuch> BesucheZurGleichenZeit(int BesuchID);
        List<IBesuch> SetInfectedBesucheZurGleichenZeit(int BesuchID);

        //Eurneuert alle als Variable gespeicherten persistences via Load()
        void refreshPersistences();
        void Delete14TageAlteBesuche();
        void EditUser(int BesucherID, String Email, string Nachname, string Vorname);

    }
}
