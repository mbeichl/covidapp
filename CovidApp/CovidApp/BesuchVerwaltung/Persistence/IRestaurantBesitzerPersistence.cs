﻿using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Et.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidApp.BesuchVerwaltung.Persistence
{
    public interface IRestaurantBesitzerPersistence
    {

        void CreateBesitzer(RestaurantBesitzer b);
        void DeleteBesitzer(string id);
        IDictionary<string, RestaurantBesitzer> LoadBesitzer();
        void SyncBesitzer(IDictionary<string, RestaurantBesitzer> besitzer);
        void UpdateBesitzer(string besitzerID, Restaurant restaurant);

    }
}
