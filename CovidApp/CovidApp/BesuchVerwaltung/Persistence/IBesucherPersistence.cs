﻿using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Et.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidApp.BesuchVerwaltung.Persistence
{
    public interface IBesucherPersistence
    {
   
      
        void CreateBesucher(IBesucher besuch);


        void DeleteBesucher(int id);

        IDictionary<int, IBesucher> LoadBesucher();

        /// <summary>
        /// Lädt Besucher aber mit Restaurants und Besuchen
        /// </summary>
        /// <returns></returns>
        IDictionary<int, IBesucher> LoadBesucherEager();


        void SyncBesucher(IDictionary<int, IBesucher> besuche);

        void UpdateBesucher(int besucherID, Besuch b);
        void UpdateBesucher(int besucherID, String Email, String Nachname, String Vorname);

        /// <summary>
        /// updated covid Status und gibt False zurück, falls User mit der ID nicht existiert oder TestDatum älter als 14 Tage
        /// </summary>
        bool CovidUpdate(int besucherID, bool CovidStatus, DateTime? PositveTestDate);


        void DeleteAlleBesucher();
    }
}
