﻿using CovidApp.BesuchVerwaltung.Dt;
using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Et.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidApp.BesuchVerwaltung.Persistence
{
    public interface IBesuchPersistence 

    {
        /// <summary>
        /// Legt neuen Besuch persistent an
        /// </summary>
        /// <param name="besuch"></param>
        void CreateBesuch(IBesuch besuch);

        /// <summary>
        /// Löscht einen bestimmten Besuch nach ID
        /// </summary>
        /// <param name="id"></param>
        void DeleteBesuch(int id);

        /// <summary>
        /// Lädt alle persistenten Besuche
        /// </summary>
        /// <returns></returns>
        IDictionary<int, IBesuch> LoadBesuche();

        /// <summary>
        /// Synchroniziert persistenz mit liste
        /// </summary>
        /// <param name="besuche"></param>
        void SyncBesuche(IDictionary<int, IBesuch> besuche);

        /// <summary>
        /// Updated den Besuch
        /// </summary>
        /// <param name="besuchID"></param>
        /// <param name="restaurantID"></param>
        /// <param name="anfang"></param>
        /// <param name="ende"></param>
        void UpdateBesuch(int besuchID, string restaurantID, DateTime? anfang, DateTime? ende);


        /// <summary>
        /// Löscht alle Besuche aus persistenter Ablage
        /// </summary>
        void DeleteAlleBesuche();
    }
}
