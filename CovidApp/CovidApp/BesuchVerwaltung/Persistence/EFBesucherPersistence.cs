﻿using CovidApp.BesuchVerwaltung.Dt;
using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Et.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidApp.BesuchVerwaltung.Persistence
{
    public class EFBesucherPersistence : IBesucherPersistence
    {
       

        public void CreateBesucher(IBesucher besucher)
        {
            using (BesuchContext db = new BesuchContext())
            {
                db.Besucher.Add((Besucher)besucher);
                db.SaveChanges();
            }
        }

        public void DeleteAlleBesucher()
        {
            using (BesuchContext db = new BesuchContext())
            {
                foreach (Besucher b in db.Besucher)
                {
                    db.Besucher.Remove(b);
                }
                db.SaveChanges();
            }
        }

        public void DeleteBesucher(int id)
        {
            using (BesuchContext db = new BesuchContext())
            {
                IBesucher b = db.Besucher.Find(id);
                if (b != null)
                {
                    //TODO: BesuchPersistenceEF - Abhängige Entitytypen löschen

                    db.Besucher.Remove((Besucher)b);
                    db.SaveChanges();
                }
                else
                {
                    //TODO: Log Persistenz
                    Console.WriteLine("Delete geht nicht");
                }
            }
        }

        public IDictionary<int, IBesucher> LoadBesuche()
        {
            using (BesuchContext db = new BesuchContext())
            {
                IDictionary<int, IBesucher> liste = new SortedList<int, IBesucher>();

                IEnumerable<IBesucher> besucher = db.Besucher.ToList();

                foreach (IBesucher b in besucher)
                {
                    liste.Add(b.ID, b);
                }

                return liste;
            }
        }

        public void SyncBesucher(IDictionary<int, IBesucher> besucher)
        {

            using (BesuchContext db = new BesuchContext())
            {
                foreach (KeyValuePair<int, IBesucher> b in besucher)
                {
                    //Nur wenn es noch nicht drinnen ist, addet es
                    if (db.Besucher.Find(b.Value.ID) != null)
                    {
                        db.Besucher.Add((Besucher)b.Value);
                    }
                }
                foreach (IBesucher b in db.Besucher)  //schaut ob es besuche gibt, die in der Datenbank sind, aber nicht im übergegbenen Dictonary

                {
                    if (!besucher.ContainsKey(b.ID))
                    {
                        db.Besucher.Remove((Besucher)b);
                    }
                }


                db.SaveChanges();
            }
        }

        public void UpdateBesucher(int besucherID,string Nachname, bool CovidStatus, DateTime PositveTestDate, string Email)
        {

            using (BesuchContext db = new BesuchContext())
            {
                IBesucher b = db.Besucher.Find(besucherID);
                if (b != null)
                {

                    if (!(Nachname =="")) b.Nachname = Nachname;

                    if (!(PositveTestDate == DateTime.MinValue)) b.PositveTestDate = PositveTestDate;

                    if (!(Email == "")) b.Email = Email;
                    b.CovidStatus = CovidStatus;

                }

            }
        }
        public void CovidUpdate(int besucherID, bool CovidStatus, DateTime PositveTestDate)
        {
            using (BesuchContext db = new BesuchContext())
            {
                IBesucher b = db.Besucher.Find(besucherID);
                if (b != null)
                {


                    if (!(PositveTestDate == DateTime.MinValue)) b.PositveTestDate = PositveTestDate;
                    b.CovidStatus = CovidStatus;


                }

            }

        }
    }
    }



