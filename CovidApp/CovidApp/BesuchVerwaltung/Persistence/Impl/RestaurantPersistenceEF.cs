﻿using CovidApp.BesuchVerwaltung.Dt;
using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Et.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidApp.BesuchVerwaltung.Persistence
{
    public class RestaurantPersistenceEF : IRestaurantPersistence
    {

        public void CreateRestaurant(IRestaurant restaurant)
        {
            using (BesuchContext db = new BesuchContext())
            {
                db.Restaurants.Add((Restaurant)restaurant);
                db.SaveChanges();
            }
        }

        public void DeleteRestaurant(int id)
        {
            using (BesuchContext db = new BesuchContext())
            {
                IRestaurant r = db.Restaurants.Find(id);
                if (r != null)
                {
                    //TODO: RestaurantPersistenceEF - Abhängige Entitytypen löschen

                    db.Restaurants.Remove((Restaurant)r);
                    db.SaveChanges();
                }
                else
                {
                    //TODO: Log Persistenz
                    Console.WriteLine("Delete geht nicht");
                }
            }
        }

        public void DeleteAllRestaurants()
        {
            using (BesuchContext db = new BesuchContext())
            {
                foreach (Restaurant r in db.Restaurants)
                {
                    db.Restaurants.Remove(r);
                }
                db.SaveChanges();
            }
        }

        public IDictionary<int, IRestaurant> LoadRestaurants()
        {
            IDictionary<int, IRestaurant> liste = new SortedList<int, IRestaurant>();
            using (BesuchContext db = new BesuchContext())
            {
                IEnumerable<Restaurant> rest = db.Restaurants.ToList();

                foreach (Restaurant r in rest)
                {
                    liste.Add(r.RestaurantID, r);
                }
            }
            return liste;
        }

        public void SyncRestaurants(IDictionary<int, Restaurant> restaurants)
        {
            using (BesuchContext db = new BesuchContext())
            {
                foreach (KeyValuePair<int, Restaurant> r in restaurants)
                {
                    //Nur wenn es noch nicht drinnen ist, addet es
                    if (db.Restaurants.Find(r.Value.RestaurantID) != null)
                    {
                        db.Restaurants.Add((Restaurant)r.Value);
                    }
                }
                foreach (IRestaurant r in db.Restaurants)  //schaut ob es Restaurants gibt, die in der Datenbank sind, aber nicht im übergegbenen Dictonary

                {
                    if (!restaurants.ContainsKey(r.RestaurantID))
                    {
                        db.Restaurants.Remove((Restaurant)r);
                    }
                }


                db.SaveChanges();
            }

        }

        public void UpdateRestaurants(int resID, int besitzerID, string Adresse)
        {
            using (BesuchContext db = new BesuchContext())
            {
                Restaurant r = db.Restaurants.Find(resID);
                if (r != null)
                {
                    RestaurantBesitzer b = db.Restaurantbesitzer.Find(besitzerID);
                    r.Adresse = Adresse;
                }

            }
        }

        public void UpdateRestaurants(int resID, Besuch b)
        {
            using (BesuchContext db = new BesuchContext())
            {
                Restaurant restaurant = db.Restaurants.Find(resID);
                if (restaurant != null)
                {
                    restaurant.Besuche.Add(b);
                }
                db.SaveChanges();
            }
        }
    }
}