﻿using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Et.Impl;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidApp.BesuchVerwaltung.Persistence
{
    public class BesuchPersistenceEF : IBesuchPersistence
    {
        public void CreateBesuch(IBesuch besuch)
        {
            using (BesuchContext db = new BesuchContext())
            {
                var besuchNeu = (Besuch)besuch;
                if ((db.Restaurants.Find(besuch.RestaurantID)) == null)
                {
                    throw new Exception("Restaurant nicht gefunden");
                }
                if((db.Besucher.Find(besuch.BesucherID)) == null)
                {
                    throw new Exception("Besucher nicht gefunden");
                }
                besuchNeu.Restaurant = db.Restaurants.FirstOrDefault(r => r.RestaurantID == besuch.RestaurantID);
                besuchNeu.Besucher = db.Besucher.FirstOrDefault(b => b.BesucherID == besuch.BesucherID);
                                
                db.Besuche.Add(besuchNeu);
                    db.SaveChanges();               
            }
        }

        public void DeleteBesuch(int id)
        {
            using (BesuchContext db = new BesuchContext())
            {
                IBesuch b = db.Besuche.Find(id);
                if (b != null)
                {
                    //TODO: BesuchPersistenceEF - Abhängige Entitytypen löschen

                    db.Besuche.Remove((Besuch)b);
                    db.SaveChanges();
                }
                else
                {
                    //TODO: Log Persistenz
                    Console.WriteLine("Delete geht nicht");
                }
            }
        }

        public void DeleteAlleBesuche()
        {
            using (BesuchContext db = new BesuchContext())
            {
                foreach (Besuch b in db.Besuche)
                {
                    db.Besuche.Remove(b);
                }
                db.SaveChanges();
            }
        }

        public IDictionary<int, IBesuch> LoadBesuche()
        {
            using (BesuchContext db = new BesuchContext())
            {
                IDictionary<int, IBesuch> liste = new SortedList<int, IBesuch>();

                var besuche = db.Besuche
                                        .Include("Restaurant")
                                        .Include("Besucher")
                                        .ToList();

                foreach (Besuch b in besuche)
                {                   
                    liste.Add(b.BesuchID, b);
                }

                return liste;
            }
        }

        public void SyncBesuche(IDictionary<int, IBesuch> besuche)
        {
            using (BesuchContext db = new BesuchContext())
            {
                foreach (KeyValuePair<int, IBesuch> b in besuche)
                {
                    //Nur wenn es noch nicht drinnen ist, addet es
                    if (db.Besuche.Find(b.Value.BesuchID) != null)
                    {
                        db.Besuche.Add((Besuch)b.Value);
                    }
                }
                foreach (IBesuch b in db.Besuche)  //schaut ob es besuche gibt, die in der Datenbank sind, aber nicht im übergegbenen Dictonary

                {
                    if (!besuche.ContainsKey(b.BesuchID))
                    {
                        db.Besuche.Remove((Besuch)b);
                    }
                }


                db.SaveChanges();
            }

            //TODO: BesuchPersistence - Synchronization mit Datenbankfunktion
        }

        public void UpdateBesuch(int besuchID, string restaurantID, DateTime? anfang, DateTime? ende)
        {
            using (BesuchContext db = new BesuchContext())
            {
                IBesuch b = db.Besuche.Find(besuchID);
                if (b != null)
                {
                    Restaurant r = db.Restaurants.Find(restaurantID);
                    if (r != null) b.Restaurant = r;

                    if (!(anfang is null)) b.anfang = (DateTime)anfang;
                    if (!(ende is null)) b.ende = (DateTime)ende;
                }

            }
        }
    }
}
