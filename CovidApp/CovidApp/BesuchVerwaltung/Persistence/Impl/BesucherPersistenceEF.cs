﻿using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Et.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidApp.BesuchVerwaltung.Persistence
{
    public class BesucherPersistenceEF : IBesucherPersistence
    {
        public void CreateBesucher(IBesucher besucher)
        {
            using (BesuchContext db = new BesuchContext())
            {
                db.Besucher.Add((Besucher)besucher);
                db.SaveChanges();
            }
        }

        public void DeleteAlleBesucher()
        {
            using (BesuchContext db = new BesuchContext())
            {
                foreach (Besucher b in db.Besucher)
                {
                    db.Besucher.Remove(b);
                }
                db.SaveChanges();
            }
        }

        public void DeleteBesucher(int id)
        {
            using (BesuchContext db = new BesuchContext())
            {
                IBesucher b = db.Besucher.Find(id);
                if (b != null)
                {
                    //TODO: BesuchPersistenceEF - Abhängige Entitytypen löschen

                    db.Besucher.Remove((Besucher)b);
                    db.SaveChanges();
                }
                else
                {
                    //TODO: Log Persistenz
                    Console.WriteLine("Delete geht nicht");
                }
            }
        }

        public IDictionary<int, IBesucher> LoadBesucher()
        {
            using (BesuchContext db = new BesuchContext())
            {
                IDictionary<int, IBesucher> liste = new SortedList<int, IBesucher>();

                var besucher = db.Besucher                                            
                                            .Include("Besuche")
                                            .ToList();

                foreach (Besucher b in besucher)
                {
                    liste.Add(b.BesucherID, (Besucher)b);
                }

                return liste;
            }
        }

        public void SyncBesucher(IDictionary<int, IBesucher> besucher)
        {

            using (BesuchContext db = new BesuchContext())
            {
                foreach (KeyValuePair<int, IBesucher> b in besucher)
                {
                    //Nur wenn es noch nicht drinnen ist, addet es
                    if (db.Besucher.Find(b.Value.BesucherID) != null)
                    {
                        db.Besucher.Add((Besucher)b.Value);
                    }
                }
                foreach (IBesucher b in db.Besucher)  //schaut ob es besuche gibt, die in der Datenbank sind, aber nicht im übergegbenen Dictonary

                {
                    if (!besucher.ContainsKey(b.BesucherID))
                    {
                        db.Besucher.Remove((Besucher)b);
                    }
                }


                db.SaveChanges();
            }
        }

        public void UpdateBesucher(int besucherID, Besuch b)
        {            
            using (BesuchContext db = new BesuchContext())
            {
                Besucher besucher = db.Besucher.Find(besucherID);
                if (besucher != null)
                {
                    besucher.Besuche.Add(b);                  
                }
                db.SaveChanges();
            }
        }
        public void UpdateBesucher(int besucherID, String Email, String Nachname, String Vorname)
        {
            using (BesuchContext db = new BesuchContext())
            {
                Besucher besucher = db.Besucher.Find(besucherID);
                if (besucher != null)
                {
                    if (Email != "") {
                        besucher.Email = Email;
                    }
                    if (Nachname != "")
                    {
                        besucher.Nachname = Nachname;
                    }
                    if (Vorname != "")
                    {
                        besucher.Vorname = Vorname;
                    }

                }
                db.SaveChanges();
            }
        }
        public bool CovidUpdate(int besucherID, bool CovidStatus, DateTime? PositveTestDate)
        {
            using (BesuchContext db = new BesuchContext())
            {
                Besucher b = db.Besucher.Find(besucherID);
                if (b != null)
                {
                    //Rausfinden wie alt der der Test ist
                    DateTime today = DateTime.Today;
                    TimeSpan TestAlter = today - (DateTime)PositveTestDate; //Unterschied zw. Test Datum und Heute
                       double TestAlterInTagen = TestAlter.TotalDays;
                    if (PositveTestDate != DateTime.MinValue && TestAlterInTagen <= 14 && TestAlterInTagen >= 0) //Überprüfen ob der test älter als 14 Tage (also insignifkant is)
                    {
                        b.PositiveTestDate = PositveTestDate;
                        b.CovidStatus = CovidStatus;


                        db.SaveChanges();
                        return true;

                 
                    }
                    else
                    {  
                        return false;
                    }

                }
                else
                {

                    return false;                }

            }

        }

        public IDictionary<int, IBesucher> LoadBesucherEager()
        {
            throw new NotImplementedException();
        }
    }
}
