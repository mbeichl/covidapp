﻿using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Et.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidApp.BesuchVerwaltung.Persistence
{
    public class RestaurantBesitzerPersistenceEF : IRestaurantBesitzerPersistence
    {
        public void CreateBesitzer(RestaurantBesitzer b)
        {
            using (BesuchContext db = new BesuchContext())
            {
                RestaurantBesitzer rb = db.Restaurantbesitzer.Find(b.email);
                if (rb == null)
                {
                    db.Restaurantbesitzer.Add((RestaurantBesitzer)b);
                    db.SaveChanges();
                    Logger.log.Info("Neuer Besitzer angelegt");
                }
            }
        }

        public void DeleteBesitzer(string id)
        {
            using (BesuchContext db = new BesuchContext())
            {
                IRestaurantBesitzer b = db.Restaurantbesitzer.Find(id);
                if (b != null)
                {
                    //TODO: BesitzerPersistence - Abhängige Entitytypen löschen

                    db.Restaurantbesitzer.Remove((RestaurantBesitzer)b);
                    db.SaveChanges();
                }
                else
                {
                    //TODO: Log Persistenz
                    Console.WriteLine("Delete geht nicht");
                }
            }
        }

        public IDictionary<string, RestaurantBesitzer> LoadBesitzer()
        {
            IDictionary<string, RestaurantBesitzer> liste = new SortedList<string, RestaurantBesitzer>();
            using (BesuchContext db = new BesuchContext())
            {
                IEnumerable<IRestaurantBesitzer> besitzer = db.Restaurantbesitzer.ToList();

                foreach (RestaurantBesitzer b in besitzer)
                {
                    liste.Add(b.email, b);
                }
            }
            return liste;
        }

        public void SyncBesitzer(IDictionary<string, RestaurantBesitzer> besitzer)
        {
            using (BesuchContext db = new BesuchContext())
            {
                foreach (KeyValuePair<string, RestaurantBesitzer> b in besitzer)
                {
                    //Nur wenn es noch nicht drinnen ist, addet es
                    if (db.Restaurantbesitzer.Find(b.Value.email) != null)
                    {
                        db.Restaurantbesitzer.Add((RestaurantBesitzer)b.Value);
                    }
                }
                foreach (IRestaurantBesitzer b in db.Restaurantbesitzer)  //schaut ob es besuche gibt, die in der Datenbank sind, aber nicht im übergegbenen Dictonary

                {
                    if (!besitzer.ContainsKey(b.email))
                    {
                        db.Restaurantbesitzer.Remove((RestaurantBesitzer)b);
                    }
                }


                db.SaveChanges();
            }

            //TODO: RestaurantBesitzer - Synchronization mit Datenbankfunktion
        }

        public void UpdateBesitzer(string besitzerID, Restaurant restaurant)
        {            
            using (BesuchContext db = new BesuchContext())
            {

                RestaurantBesitzer b = db.Restaurantbesitzer.Find(besitzerID);
                if (b == null) throw new Exception("Restaurantbesitzer not known");
                try
                {
                    b.Restaurants.Add(restaurant);
                } catch(NullReferenceException e)
                {
                    Logger.log.Error("Restaurant relationship failed" + e.Message);
                    throw e;
                }
               
                db.SaveChanges();
            }
        }
    }
}

