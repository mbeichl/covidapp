﻿using CovidApp.BesuchVerwaltung.Dt;
using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Et.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidApp.BesuchVerwaltung.Persistence
{
    public interface IRestaurantPersistence
    {

        /// <summary>
        /// Legt neuen Besuch persistent an
        /// </summary>
        /// <param name="besuch"></param>
        void CreateRestaurant(IRestaurant besuch);

        /// <summary>
        /// Löscht einen bestimmten Besuch nach ID
        /// </summary>
        /// <param name="id"></param>
        void DeleteRestaurant(int id);

        /// <summary>
        /// Lädt alle persistenten Besuche
        /// </summary>
        /// <returns></returns>
        IDictionary<int, IRestaurant> LoadRestaurants();

        /// <summary>
        /// Synchroniziert persistenz mit liste
        /// </summary>
        /// <param name="besuche"></param>
        void SyncRestaurants(IDictionary<int, Restaurant> restaurants);

        /// <summary>
        /// Updatet besuch mit sachen
        /// </summary>
        /// <param name="besuch"></param>
        /// <param name="RestaurantID"></param>
        /// <param name="zeitspanne"></param>
        /// <param name="Datum"></param>
        /// <param name="Bedienung"></param>
        void UpdateRestaurants(int resID, int besitzerID, string Adresse);
        /// <summary>
        /// Updated Restaurant mit Besuch
        /// </summary>
        /// <param name="resID"></param>
        /// <param name="b"></param>
        void UpdateRestaurants(int resID, Besuch b);


        /// <summary>
        /// Löscht alle Besuche aus persistenter Ablage
        /// </summary>
        void DeleteAllRestaurants();
    }
}
