﻿using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Et.Impl;
using CovidApp.BesuchVerwaltung;

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Core.Metadata.Edm;

namespace CovidApp.BesuchVerwaltung.Persistence
{
    public class BesuchContext : DbContext
    {
        public BesuchContext() : base("Covid-Datenbank")
        {
            //Neue Datenbank wenn das Modell sich ändert
            Database.SetInitializer<BesuchContext>(new DropCreateDatabaseIfModelChanges<BesuchContext>());    

            //Database.SetInitializer<BesuchContext>(new CustomInitializer<BesuchContext>()); //nur temporär!
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Besucher>()
                .HasMany<Besuch>(b => b.Besuche)
                .WithRequired(s => s.Besucher)
                .HasForeignKey(s => s.BesucherID)
                .WillCascadeOnDelete();
            modelBuilder.Entity<Restaurant>()
                .HasMany<Besuch>(r => r.Besuche)
                .WithRequired(s => s.Restaurant)
                .HasForeignKey(s => s.RestaurantID);
            modelBuilder.Entity<RestaurantBesitzer>()
                .HasMany<Restaurant>(r => r.Restaurants)
                .WithRequired(s => s.RestaurantBesitzer)
                .HasForeignKey(s => s.RestaurantBesitzerId);

        }


        public virtual DbSet<Besuch> Besuche { get; set; }
        public virtual DbSet<Besucher> Besucher { get; set; }
        public virtual DbSet<Restaurant> Restaurants { get; set; }
        public virtual DbSet<RestaurantBesitzer> Restaurantbesitzer { get; set; }

        /// <summary>
        /// Custom Initializer damit man die Datenbank überschreiben kann während man arbeitet. Sonst muss man die Connection droppen und VS neustarten.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public class CustomInitializer<T> : DropCreateDatabaseAlways<BesuchContext>
        {
            public override void InitializeDatabase(BesuchContext context)
            {
                context.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction
                    , string.Format("ALTER DATABASE [{0}] SET SINGLE_USER WITH ROLLBACK IMMEDIATE", context.Database.Connection.Database));

                base.InitializeDatabase(context);
            }
        }
    }
}



