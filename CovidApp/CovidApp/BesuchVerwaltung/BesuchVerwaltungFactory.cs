﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Persistence;
using CovidApp.BesuchVerwaltung.Uc;
using CovidApp.BesuchVerwaltung.Uc.Impl;

namespace CovidApp.BesuchVerwaltung
{
    public class BesuchVerwaltungFactory
    {
        private BesuchVerwaltungImpl verwaltung;


        public BesuchVerwaltungFactory(IBesuchPersistence besuchPersistence, IRestaurantPersistence restaurantPersistence, 
                                         IBesucherPersistence besucherPersistence, IRestaurantBesitzerPersistence besitzerPersistence)

        {
            verwaltung = new BesuchVerwaltungImpl(besuchPersistence, restaurantPersistence, besucherPersistence, besitzerPersistence);
        }
        public IVerwaltungAdmin getBesuchAdmin()
        {
            return verwaltung;
        }
        public IVerwaltungModule getVerwaltungModule()
        {
            return verwaltung;
        }

    }
}
