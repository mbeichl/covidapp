﻿using CovidApp.AccountVerwaltung;
using CovidApp.AccountVerwaltung.Persistence;
using CovidApp.AccountVerwaltung.Util;
using CovidApp.AccountVerwaltung.Util.Impl;
using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Et.Impl;
using CovidApp.BesuchVerwaltung.Persistence;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace CovidApp.BesuchVerwaltung.Uc.Impl
{
    public class AdminHelper : IAdminHelper
    {
        private IDictionary<int, UserAccount> accounts = new SortedList<int, UserAccount>();

        private IAccountPersistence persistence;

        public AdminHelper(IAccountPersistence persistence)
        {
            this.persistence = persistence;
            accounts = persistence.LoadAccounts();
        }

        public List<RestaurantBesitzer> ReadRestaurantBesitzerFromFile(string str)
        {
            List<RestaurantBesitzer> res = new List<RestaurantBesitzer>();
            using (StreamReader reader = new StreamReader(str))
            {
                string text;
                while ((text = reader.ReadLine()) != null)
                {
                    Regex CSVreg = new Regex((",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))"));

                    string[] felder = CSVreg.Split(text);
                    if (felder.Length > 4)
                    {
                        Logger.log.Error("Falsches Format! Pfad: " + str);
                        throw new Exception("Datei falsches Format");
                    }
                    for (int i = 0; i < felder.Length; i++)
                    {
                        felder[i] = felder[i].TrimStart(' ', '"');
                        felder[i] = felder[i].TrimEnd('"');
                    }

                    RestaurantBesitzer rb = new RestaurantBesitzer(email: felder[0], adr: felder[1], name: felder[2], telefonnummer: felder[3]);
                    res.Add(rb);
                    //Logger.log.Info(rb.name + " " + rb.adresse);
                    //Logger.log.Info(felder[0]+ " " + felder[1]+ " " + felder[2]+ " " + felder[3]);
                }
                Logger.log.Info("Restaurantbesitzer hinzugefügt von Pfad: " + str);
                return res;
            }
        }
        public SortedDictionary<Restaurant, string> ReadRestaurantsFromFile(string str)
        {
            var list = new SortedDictionary<Restaurant, string>();
            using (StreamReader reader = new StreamReader(str))
            {
                string text;
                while ((text = reader.ReadLine()) != null)
                {
                    Regex CSVreg = new Regex((",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))"));

                    string[] felder = CSVreg.Split(text);

                    Restaurant r = new Restaurant(felder[0], felder[1], felder[2], felder[3]);
                    list.Add(r, felder[4]);
                    //Logger.log.Info("Restaurantdaten: " + felder[0] + " " + felder[1] + " " + felder[2] + " " + felder[3] + " Key: " + felder[4]);
                }
                Logger.log.Info("Restaurants gelesen von Pfad: " + str);
                return list;
            }
        }


        public IUserAccount getAccount(String input)
        {
            return accounts.FirstOrDefault(x=> x.Value.email == input).Value;
        }
        public IUserAccount getAccount(int input)
        {
            return accounts[input];
        }
        public void elevateAccount(int IDinput, int Level)
        {
            persistence.UpdateAccountLvl(IDinput, Level);
        }


        public void downloadDB(string path)
        {
            using (BesuchContext db = new BesuchContext())
            {
                var BesucherList = db.Besucher.ToList();

                var xml = new XElement("Besucher", BesucherList.Select(b => new XElement("Besucher",
                                               new XAttribute("Vorname", b.Vorname),
                                               new XAttribute("Nachname", b.Nachname),
                                               new XAttribute("EMailAdresse", b.Email))));
                
                string combined = Path.Combine(path, @"Besucher.xaml");
                Logger.log.Info("Datei gespeichert. Filepath: ["+combined+"]");
                xml.Save(combined);
            }
        }

    }
}
