﻿using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Et.Impl;
using CovidApp.BesuchVerwaltung.Persistence;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CovidApp.BesuchVerwaltung.Uc.Impl
{

    public class BesuchVerwaltungImpl : IVerwaltungModule, IVerwaltungAdmin
    {
        #region Persistence & Dictionary
        private IBesuchPersistence besuchPersistence;
        private IRestaurantPersistence restaurantPersistence;
        private IBesucherPersistence besucherPersistence;
        private IRestaurantBesitzerPersistence besitzerPersistence;

        private IDictionary<int, IBesuch> besuche = new SortedList<int, IBesuch>();
        private IDictionary<int, IRestaurant> restaurants = new SortedList<int, IRestaurant>();
        private IDictionary<int, IBesucher> besucher = new SortedList<int, IBesucher>();
        private IDictionary<string, RestaurantBesitzer> besitzer = new SortedList<string, RestaurantBesitzer>();
        #endregion

        public BesuchVerwaltungImpl(IBesuchPersistence besuchPersistence, IRestaurantPersistence restaurantPersistence,
                                    IBesucherPersistence besucherPersistence, IRestaurantBesitzerPersistence besitzerPersistence)
        {
            this.restaurantPersistence = restaurantPersistence;
            this.besuchPersistence = besuchPersistence;
            this.besucherPersistence = besucherPersistence;
            this.besitzerPersistence = besitzerPersistence;

            besuche = besuchPersistence.LoadBesuche();
            besucher = besucherPersistence.LoadBesucher();
            restaurants = restaurantPersistence.LoadRestaurants();
            besitzer = besitzerPersistence.LoadBesitzer();
        }


        #region Besuch
        public void AddBesuch(IBesucher besucher, Restaurant Restaurant, DateTime? Anfang, DateTime? Ende)
        {
            IBesuch b = new Besuch((Besucher)besucher, Restaurant, Anfang, Ende);
            besuchPersistence.CreateBesuch(b);
            besuche = besuchPersistence.LoadBesuche();
        }

        public void DeleteBesuch(int id)
        {
            besuchPersistence.DeleteBesuch(id);
            besuche = besuchPersistence.LoadBesuche();
        }
        public List<IBesuch> ListBesuche()
        {

            List<IBesuch> IBesucheList = new List<IBesuch>();

            foreach (IBesuch b in besuchPersistence.LoadBesuche().Values)
            {
                IBesucheList.Add(b);
            }

            return IBesucheList;
        }

        public IDictionary<int, IBesuch> DictionaryBesuche()
        {
            return besuchPersistence.LoadBesuche();
        }

        public IBesuch BesuchAnlegen(IBesucher besucher, IRestaurant restaurant, DateTime datumVon, DateTime datumBis)
        {
            var b = new Besuch() { BesucherID = besucher.BesucherID, RestaurantID = restaurant.RestaurantID, anfang = datumVon, ende = datumBis };
            besuchPersistence.CreateBesuch(b);
            return b;
        }

        public List<IBesuch> getBesucheFromBesucher(int BesucherID)
        {
            List<IBesuch> BesucheList = new List<IBesuch>();
            foreach (var b in besuchPersistence.LoadBesuche().Values.Where(b => b.BesucherID == BesucherID))
            {
                BesucheList.Add(b);
            }
            return BesucheList;

        }


        #endregion

        #region Restaurant
        public void AddRestaurant(Restaurant r)
        {
            restaurantPersistence.CreateRestaurant(r);
            restaurants = restaurantPersistence.LoadRestaurants();
        }
        public void DeleteRestaurant(int id)
        {
            restaurantPersistence.DeleteRestaurant(id);
            restaurants = restaurantPersistence.LoadRestaurants();
        }
        public IList<Restaurant> ListRestaurants()
        {
            IList<Restaurant> liste = (IList<Restaurant>)restaurants.Values;
            return liste;
        }
        public IDictionary<int, IRestaurant> DictionaryRestaurants()
        {
            return restaurantPersistence.LoadRestaurants();
        }
        public IRestaurant getRestaurant(int ID)
        {
            return restaurants[ID];
        }
        public List<Restaurant> getRestaurant(String name)
        {
            name = name.Trim(new Char[] { ' ', '*', '.' });
            List<Restaurant> re = new List<Restaurant>();
            foreach (Restaurant r in restaurants.Values)
            {
                if (r.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase))
                    re.Add(r);
            }
            return re;
        }
        public List<IRestaurant> getRestaurantFromBesitzer(String BesitzerID)
        {
            List<IRestaurant> re = (
                from r in restaurants.Values
                where r.RestaurantBesitzerId == BesitzerID
                select r).ToList();
            return re;
        }
        public void AddBesuchToRestaurant(int resID, Besuch b)
        {
            restaurantPersistence.UpdateRestaurants(resID, b);
            this.restaurants = restaurantPersistence.LoadRestaurants();
        }


        #endregion

        #region Besucher
        public IBesucher AddBesucher(string vorname, string nachname, string email)
        {
            Besucher b = new Besucher(vorname, nachname, email);
            besucherPersistence.CreateBesucher(b);
            besucher = besucherPersistence.LoadBesucher();
            return b;
        }
        public void AddBesuchToBesucher(int besucherID, Besuch b)
        {
            besucherPersistence.UpdateBesucher(besucherID, b);
            this.besucher = besucherPersistence.LoadBesucher();
        }
        public void DeleteBesucher(int id)
        {
            CovidApp.Logger.log.Info("Delete Besucher()");
            CovidApp.Logger.log.Info("Zunächst mit foreach alle seine Besuche loeschen ");

            foreach (IBesuch b in DictionaryBesucher()[id].DictBesuche().Values)
            {
                DeleteBesuch(b.BesucherID);
            }

            besucherPersistence.DeleteBesucher(id);
            
            besucher = besucherPersistence.LoadBesucher();
        }
        public List<IBesucher> ListBesucher()
        {
            List<IBesucher> IBesucherList = new List<IBesucher>();

            foreach (IBesucher b in besucherPersistence.LoadBesucher().Values)
            {
                IBesucherList.Add(b);
            }

            return IBesucherList;
        }
        public IBesucher getBesucher(int ID)
        {
            return besucher[ID];
        }

        public IDictionary<int, IBesucher> DictionaryBesucher()
        {
            return besucherPersistence.LoadBesucher();
        }

        #endregion

        #region RestaurantBesitzer
        public IRestaurantBesitzer AddRestaurantBesitzer(RestaurantBesitzer rb)
        {
            besitzerPersistence.CreateBesitzer(rb);
            besitzer = besitzerPersistence.LoadBesitzer();
            return rb;
        }
        public void DeleteRestaurantBesitzer(string id)
        {
            besitzerPersistence.DeleteBesitzer(id);
            besitzer = besitzerPersistence.LoadBesitzer();
        }
        public IList<RestaurantBesitzer> ListRestaurantBesitzer()
        {
            IList<RestaurantBesitzer> liste = (IList<RestaurantBesitzer>)besitzer.Values;
            return liste;
        }
        public IRestaurantBesitzer getRestaurantBesitzer(string ID)
        {
            return besitzer[ID];
        }
        public void AddRestaurantToBesitzer(string besitzer, Restaurant r)
        {
            besitzerPersistence.UpdateBesitzer(besitzer, r);
            this.besitzer = besitzerPersistence.LoadBesitzer();
            this.restaurants = restaurantPersistence.LoadRestaurants();
        }

        #endregion



        public List<IBesuch> getSelbeZeitspanne(IBesuch other)
        {
            IDictionary<int, IBesuch> AlleBesuche = besuchPersistence.LoadBesuche();
            List<IBesuch> timeoverlaps = new List<IBesuch>();
            foreach (IBesuch b in AlleBesuche.Values)
            {
                if (b.overlapsZeitspanne(other))
                {
                    timeoverlaps.Add(b);
                }
            }
            return timeoverlaps;
        }


        public bool meldePositiv(int id, DateTime? PositveTestDate)
        {
            return besucherPersistence.CovidUpdate(id, true, PositveTestDate);
        }

        public void AlleBesuchDatenVergleichen()
        {
            CovidApp.Logger.log.Info("admin.AlleBesuchDatenVergleichen()");

            List<IBesuch> besuche = ListBesuche();
            List<IBesucher> besucher = ListBesucher();

            CovidApp.Logger.log.Info("Alle Besuche durchlaufen");
            foreach (IBesuch b in ListBesuche())
            {
                if (DictionaryBesucher()[b.BesucherID].CovidStatus == true)
                {
                    CovidApp.Logger.log.Info("DictionaryBesucher()[b.BesucherID].CovidStatus = true = > Alle Bes zur gleichen zeit: SetInfectedZurgleichenZeit");

                    SetInfectedBesucheZurGleichenZeit(b.BesuchID);
                }
            }













        }
        public List<IBesuch> SetInfectedBesucheZurGleichenZeit(int BesuchID)
        {
            List<IBesuch> re = new List<IBesuch>();

            IBesuch vgl = DictionaryBesuche()[BesuchID];

            foreach (IBesuch b in ListBesuche())
            {
                if (vgl.overlapsZeitspanneAndRestaurant(b))
                {

                    meldePositiv(b.BesucherID, b.anfang);
                    re.Add(b);
                }
            }

            return re;
        }



       

        
        public List<IBesuch> BesucheZurGleichenZeit(int BesuchID)
        {
            List<IBesuch> re = new List<IBesuch>();

            IBesuch vgl = DictionaryBesuche()[BesuchID];

            foreach (IBesuch b in ListBesuche())
            {
                if (vgl.overlapsZeitspanneAndRestaurant(b))
                {
                    re.Add(b);
                }
            }

            return re;

        }


        public List<IBesuch> ReadBesucheFromFile(string str)
        {
            var deDE = new CultureInfo("de-DE");
            DateTime? getDatumFromString(string eingabe, DateTime? date)
            {
                DateTime dt;
                string[] formats = { "HH:mm" };

                if (!DateTime.TryParseExact(eingabe, formats, deDE, System.Globalization.DateTimeStyles.None, out dt))
                {
                    return null;
                }
                TimeSpan time = dt.TimeOfDay;
                if (dt.TimeOfDay.TotalMinutes > 1440)
                {
                    return null;
                }
                date = date + time;
                return date;
            }

            List<IBesuch> bes = new List<IBesuch>();

            var lines = File.ReadLines(str);

            foreach (var line in lines)
            {
                var array = line.Split(',');
                Logger.log.Info("Line read: " + string.Join(", ", array));
                DateTime datum = DateTime.ParseExact(array[5], "MM.dd.yyyy", deDE).Date;

                DateTime? anfang = getDatumFromString(array[3], datum);
                DateTime? ende = getDatumFromString(array[4], datum);
                var b = new Besucher() { Vorname = array[0], Nachname = array[1], Email = array[2] };
                bes.Add((new Besuch() { anfang = anfang, ende = ende, Besucher = b }));
                Logger.log.Info("Besuch gelesen: " + besucher + " " + anfang.ToString() + " " + ende.ToString());
            }

            Logger.log.Info("Besuche gelesen von Pfad: " + str);

            return bes;
        }


        public void refreshPersistences()
        {
            besuche = besuchPersistence.LoadBesuche();
            besucher = besucherPersistence.LoadBesucher();
            restaurants = restaurantPersistence.LoadRestaurants();
            besitzer = besitzerPersistence.LoadBesitzer();
        }

        public void Delete14TageAlteBesuche()
        {

            if (ListBesuche() != null)
            {

                List<IBesuch> besuche = ListBesuche();



                foreach (Besuch b in besuche)
                {
                    //Rausfinden wie alt der der Besuch ist
                    DateTime today = DateTime.Today;
                    TimeSpan BesuchAlter = today - b.getDatum();
                    double BesuchAlterInTagen = BesuchAlter.TotalDays;
                    bool gabsDaten = false;
                    if (BesuchAlterInTagen > 14) //Löschen wenn älter als 14 Tage
                    {
                        gabsDaten = true;
                        DeleteBesuch(b.BesuchID);
                    }

                }
            }
        }

        public void EditUser(int BesucherID,String Email, string Nachname,string Vorname)
        {
            if (!Email.Contains('@'))
            {              
                besucherPersistence.UpdateBesucher(BesucherID, Email, Nachname, Vorname);
            }
            else
            {
                throw new FormatException("Email muss ein @ enthalten");
            }
        }
    }
}

