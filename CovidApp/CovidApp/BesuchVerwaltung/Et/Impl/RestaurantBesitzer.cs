﻿using CovidApp.AccountVerwaltung.Util.Impl;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CovidApp.BesuchVerwaltung.Et.Impl
{
    [Serializable]
    [Table("RestaurantBesitzer")]
    public class RestaurantBesitzer :  IRestaurantBesitzer
    {
        #region Konstruktor
        public RestaurantBesitzer() { }
        public RestaurantBesitzer(string email, string adr, string name, string telefonnummer)
        {
            this.name = name;
            this.adresse = adr;
            this.email = email;
            this.telefonnummer = telefonnummer;
            //this.RestaurantbesitzerID = Guid.NewGuid().ToString();
        }

        #endregion


        //public string RestaurantbesitzerID { get; set; }
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string email { get; set; }
        public virtual ICollection<Restaurant> Restaurants { get; set; } //DB 1 : n

        public string name { get; set; }
        public string adresse { get; set; }        
        public string telefonnummer { get; set; }

        public override string ToString()
        {
            return this.name;
        }


    }
}
