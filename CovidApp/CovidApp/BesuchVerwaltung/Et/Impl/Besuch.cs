﻿using CovidApp.BesuchVerwaltung.Dt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading;
using static CovidApp.Applikation;

namespace CovidApp.BesuchVerwaltung.Et.Impl
{
    [Serializable]
    [Table("Besuche")]
    public class Besuch : IBesuch, IComparable<Besuch>
    {
        #region Konstruktor 
        public Besuch() { } //für Serialisiering (DB)

        public Besuch(Besucher besucher, Restaurant Restaurant, DateTime? Anfang, DateTime? Ende)
        {
            this.Besucher = besucher;
            this.Restaurant = Restaurant;
            this.anfang = Anfang;
            this.ende = Ende;
            this.BesucherID = Besucher.BesucherID;
        }
        public Besuch(Restaurant Restaurant, DateTime? Anfang, DateTime? Ende)
        {
            this.Restaurant = Restaurant;
            this.anfang = Anfang;
            this.ende = Ende;
        }

        #endregion


        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BesuchID { get; set; }

        public int RestaurantID { get; set; }
        public virtual Restaurant Restaurant { get; set; } ////1 Restaurant * Besuche
        public int BesucherID { get; set; }
        public virtual Besucher Besucher { get; set; } ////1 Besucher * Besuche
            
        [Column(TypeName = "datetime2")]
        public DateTime? anfang { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? ende { get; set; }

        //Methode um aus den persistenten Daten anfang und ende wieder das [NotMapped] Feld Zeitspanne Spanne zu bekommen 
        public Zeitspanne CreateZeitspanne()
        {
            Zeitspanne Spanne = new Zeitspanne((DateTime)this.anfang, (DateTime)this.ende);
            Logger.log.Info("[NotMapped]Zeitspanne wird wiederhergestellt aus persitenten Werten Anfang und Ende");
            return Spanne;
        }


        public bool overlapsZeitspanne(IBesuch b)
        {
            bool res= this.CreateZeitspanne().isOverlap(b.CreateZeitspanne());
            Logger.log.Info("Zeitspannen vergleich ergab: ["+res+"]");
            return (res);
        }
        public bool overlapsZeitspanneAndRestaurant(IBesuch b)
        {
           // bool res = this.CreateZeitspanne().isOverlap(b.CreateZeitspanne());
            if(this.RestaurantID == b.RestaurantID && this.CreateZeitspanne().isOverlap(b.CreateZeitspanne())) {
                return true;
            }
            else
            {
                return false;
            }
        }

        public DateTime getDatum()
        {
            return this.CreateZeitspanne().Anfang.Date;
        }


        public bool IsOutdated()
        {
            TimeSpan differenz = DateTime.Now.Subtract((DateTime)ende);
            if (differenz.TotalDays >= 14) return true;
            else return false;
        }

        public int CompareTo(Besuch other)
        {
           return BesuchID.CompareTo(other.BesuchID);
        }
        public override string ToString()
        {
            return "ID: " + BesuchID + " Date: " + anfang.ToString() + " - " + ende.ToString() ;
        }

        
    }
}
