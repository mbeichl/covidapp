﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CovidApp.BesuchVerwaltung.Et.Impl
{
    [Serializable]
    [Table("Besucher")]
    public class Besucher : IBesucher
    {
        #region Konstruktor
        public Besucher() { }   //für Serialisiering (DB)

        public Besucher(string vorname, string nachname, string email)
        {
            Vorname = vorname;
            Nachname = nachname;
            CovidStatus = false;
            Email = email;
        }

        #endregion 



        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BesucherID { get; set; }
        public virtual ICollection<Besuch> Besuche { get; set; }    //1 Besucher * Besuche
        public string Vorname { get; set; }
        public string Nachname { get; set; }
        public string Geburtsdatum { get; set; }
        public bool CovidStatus { get; set; }
        public string Email { get; set; }
        public DateTime? PositiveTestDate { get; set; }


        public IDictionary<int,IBesuch> DictBesuche(){
            IDictionary<int,IBesuch> re = new SortedList<int,IBesuch>();
            foreach(IBesuch b in Besuche) {

                re.Add(b.BesuchID,b);
            }
            return re;
            }

        
        public override string ToString()
        {
            return Vorname + " " + Nachname;
        }

    }      
}
