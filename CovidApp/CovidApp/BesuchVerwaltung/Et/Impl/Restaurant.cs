﻿using CovidApp.BesuchVerwaltung.Dt;
using CovidApp.BesuchVerwaltung.Uc.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading;
using System.Text.RegularExpressions;

namespace CovidApp.BesuchVerwaltung.Et.Impl
{
    [Serializable]
    [Table("Restaurants")]
    public class Restaurant : IRestaurant, IComparable
    {
        #region Konstruktor
        public Restaurant(string name, string adr,  string mail, string telefonnummer)
        {
            Name = name;
            Adresse = adr;
            Mail = mail;
            Telefonnummer = telefonnummer;
        }

        public Restaurant(string name, string adr,  string mail, string telefonnummer, RestaurantBesitzer besitzer)
        {
            Name = name;
            Adresse = adr;
            Mail = mail;
            Telefonnummer = telefonnummer;
            this.RestaurantBesitzer = besitzer;
        }

        public Restaurant() { }

        #endregion

        public virtual ICollection<Besuch> Besuche { get; set; }  //1 Restaurant * Besuche     
        public string RestaurantBesitzerId { get; set; }
        public virtual RestaurantBesitzer RestaurantBesitzer { get; set; } ////* Restaurant 1 Besitzer

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RestaurantID { get; set; }

        public String Name { get; set; }
        public String Adresse { get; set; }
        public String Mail { get; set; }
        public string Telefonnummer { get; set; }
        public int AnzahlMitarbeiter { get; set; }

        public RestaurantBesitzer getBesitzer()
        {
            return RestaurantBesitzer;
        }

        public int CompareTo(object obj)
        {
            if (obj != null && obj.GetType() != GetType())
            {
                throw new ArgumentException("Objekt muss from Typ Restaurant sein");
            }
            return CompareTo((Restaurant)obj);
        }
        public int CompareTo(Restaurant r)
        {
            if (this.RestaurantID !=0 && r.RestaurantID!=0) return RestaurantID.CompareTo(r.RestaurantID);

            if (this.Name.Equals(r.Name))
            {
                if (this.Adresse.Equals(r.Adresse)) return (string.Compare(this.Mail, r.Mail));
                return (string.Compare(this.Adresse, r.Adresse));
            }
            else
                return (string.Compare(this.Name, r.Name));
        }

        public override string ToString()
        {
            return Name;
        }

        public string getPLZ()
        {
            try
            {
                var result= Regex.Match(Adresse, @"\b[1-9][0-9]{4}\b").Value;
                return result;

            } catch
            {
                return "XXXXX";
            }
            
        }
    }

}

