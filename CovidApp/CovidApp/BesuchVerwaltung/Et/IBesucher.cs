﻿using CovidApp.BesuchVerwaltung.Dt;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

////using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CovidApp.BesuchVerwaltung.Et.Impl;

namespace CovidApp.BesuchVerwaltung.Et
{
    public interface IBesucher
    {
        String Vorname { get; set; }
        String Nachname { get; set; }
        String Geburtsdatum { get; set; }
        bool CovidStatus { get; set; }

        //gibt an and welchen Datum der von welchem ist bzw. die Begegnung , durch den/die er potenziell angesteckt wurde
        DateTime? PositiveTestDate { get; set; }


        String Email { get; set; }
        int BesucherID { get; set; }

        IDictionary<int,IBesuch> DictBesuche();

    }
}