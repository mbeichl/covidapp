﻿using CovidApp.BesuchVerwaltung.Dt;
using CovidApp.BesuchVerwaltung.Et.Impl;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidApp.BesuchVerwaltung.Et
{
    public interface IRestaurant
    {
        string RestaurantBesitzerId { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        int RestaurantID { get; set; }

        String Mail { get; set; }
        int AnzahlMitarbeiter { get; set; }

        String Name { get; set; }
        String Adresse { get; set; }
        string Telefonnummer { get; set; }
        RestaurantBesitzer getBesitzer();
        /// <summary>
        /// Sucht die PLZ aus einem Address-string
        /// </summary>
        /// <returns></returns>
        string getPLZ();
    }
}
