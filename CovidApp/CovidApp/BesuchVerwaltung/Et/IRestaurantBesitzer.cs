﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
namespace CovidApp.BesuchVerwaltung.Et
{
    public interface IRestaurantBesitzer
    {
        
        //string RestaurantbesitzerID { get; set; }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        string email { get; set; }

        string name { get; set; }

        string telefonnummer { get; set; }
        string adresse { get; set; }


    }
}
