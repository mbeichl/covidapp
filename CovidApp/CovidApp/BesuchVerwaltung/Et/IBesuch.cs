﻿using CovidApp.BesuchVerwaltung.Dt;
using CovidApp.BesuchVerwaltung.Et.Impl;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidApp.BesuchVerwaltung.Et
{
   
    public interface IBesuch
    {

        int BesuchID { get; set; }
        int BesucherID { get; set; }
        Besucher Besucher { get; set; }
        DateTime? anfang { get; set; }
        DateTime? ende { get; set; }
        Restaurant Restaurant { get; set; }
        int RestaurantID { get; set; }
        /// <summary>
        /// Gibt zurück ob ein Besuch länger als 14 Tage her war
        /// </summary>
        /// <returns></returns>
        bool IsOutdated();

        /// <summary>
        /// Gibt das reine Datum des Besuchs zurück
        /// </summary>
        /// <returns></returns>
        DateTime getDatum();
        //IKellner Bedienung { get; set; }


        /// <summary>
        /// Vergleicht die Zeitspanne und gibt TRUE wenn es einen Overlap gibt
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        bool overlapsZeitspanne(IBesuch b);
        /// <summary>
        /// Erstellt eine 'Zeitspanne' aus Anfang und Ende
        /// </summary>
        /// <returns>Zeitspanne</returns>
        /// 
        bool overlapsZeitspanneAndRestaurant(IBesuch b);
        Zeitspanne CreateZeitspanne();
    }
}
