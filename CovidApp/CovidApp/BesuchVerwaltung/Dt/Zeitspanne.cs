﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidApp.BesuchVerwaltung.Dt
{
    public class Zeitspanne   //Problemkind: NICHT primitve Datentypen scpeichern: Lösung 1: Anfangs und Endwert in DB 
    {
        public DateTime Anfang { get; }
        public DateTime Ende { get; }


        public Zeitspanne(DateTime anfang, DateTime ende)
        {
            if (anfang > ende) throw new Exception("Ungültiges Datumsformat");

            this.Anfang = anfang;
            this.Ende = ende;

        }

        public Zeitspanne()
        {
        }

        /// <summary>
        /// TimeSpan ist eine einfache Zahl die nur die Länge darstellt
        /// </summary>
        /// <returns></returns>
        public int getDuration(){
            TimeSpan T = Anfang - Ende;
            return (int)T.TotalSeconds;
        }

        /// <summary>
        /// vergleicht zwei Zeitspannen miteinander
        /// unnötig viele Methoden falls ich später noch nähere Vergleiche machen will
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool isOverlap(Zeitspanne other)
        {
            if (this == other) return true; //Equals
            else if (containsTime(other.Anfang) && containsTime(other.Ende)) return true; //Ist vollständig enthalten
            else if (containsTime(other.Anfang)) return true; //Fängt an in der Zeitspanne
            else if (containsTime(other.Ende)) return true; //Hört auf in der Zeitspanne
            else if (other.containsTime(this.Anfang) && other.containsTime(this.Ende)) return true; //Other enthält die Zeitspanne
            return 
            (Anfang.CompareTo(other.Ende) <= 0 && other.Anfang.CompareTo(Ende) <= 0); //einfacher Vergleich, alles was man eig. braucht
        }
        /// <summary>
        /// schaut ob eine Zeitpunkt in der Zeitspanne drin ist
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public bool containsTime(DateTime date)
        { 
            return (date >= this.Anfang) && (date <= this.Ende);
        }
        public override string ToString()
        {
            return Anfang.ToString() + " - " + Ende.ToString();
        }
        

        #region Operators
        public static bool operator ==(Zeitspanne r1, Zeitspanne r2)
        {
            return r1.Equals(r2);
        }

        public static bool operator !=(Zeitspanne r1, Zeitspanne r2)
        {
            return !(r1 == r2);
        }
        public override bool Equals(object obj)
        {
            if (obj is Zeitspanne)
            {
                var r1 = this;
                var r2 = (Zeitspanne)obj;
                return r1.Anfang == r2.Anfang && r1.Ende == r2.Ende;
            }
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion

    }

}
