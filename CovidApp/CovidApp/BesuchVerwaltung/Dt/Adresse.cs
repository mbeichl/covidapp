﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidApp.BesuchVerwaltung.Dt
{
    [Serializable]
    public class Adresse
    {

        //TODO: Adresse - brauchen wir set methoden?
        public int plz { get; set; }
        public string strasse { get; set; }
        public string ort { get; set; }
        public Adresse(int plz, string strasse, string ort)
        {
            this.plz = plz;
            this.strasse = strasse;
            this.ort = ort;
        }
        public Adresse(String adresse)
        {
            String[] strArr = adresse.Split(',');
            String[] strArrViertel = strArr[1].Split(' ');

            this.plz = int.Parse(strArrViertel[1]);
            this.strasse = strArr[0];
            this.ort = strArrViertel[2];
        }


        public override bool Equals(object obj)
        {
            return obj is Adresse adresse &&
                   plz == adresse.plz &&
                   strasse == adresse.strasse &&
                   ort == adresse.ort;
        }

        public override int GetHashCode()
        {
            int hashCode = 1443246221;
            hashCode = hashCode * -1521134295 + EqualityComparer<int>.Default.GetHashCode(plz);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(strasse);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ort);
            return hashCode;
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.Append(strasse);
            str.Append(" " + plz + " " + ort);
            return str.ToString();
        }
    }
}
