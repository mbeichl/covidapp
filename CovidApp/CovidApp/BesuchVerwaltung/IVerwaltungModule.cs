﻿using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Et.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidApp.BesuchVerwaltung
{
    public interface IVerwaltungModule
    {

        IRestaurant getRestaurant(int ID);


        /// <summary>
        /// Liste an Restaurants mit gleichem Namen zurückgeben zur besseren Identifizierung
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        List<Restaurant> getRestaurant(String name);
        /// <summary>
        /// Liste and Restaurants die einem bestimmten Besitzer gehören
        /// </summary>
        /// <param name="BesitzerID"></param>
        /// <returns></returns>
        List<IRestaurant> getRestaurantFromBesitzer(String BesitzerID);
        /// <summary>
        /// Liste an Besuchen zurückgeben die zur selben Zeitspanne passieren
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        List<IBesuch> getSelbeZeitspanne(IBesuch other);

        /// <summary>
        /// Findet Besucher nach ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        IBesucher getBesucher(int ID);

        /// <summary>
        /// Liste aller persönlichen Besuche
        /// </summary>
        /// <param name="BesucherID"></param>
        /// <returns></returns>

        List<IBesuch> getBesucheFromBesucher(int BesucherID);

        List<IBesuch> ReadBesucheFromFile(string str);
        /// <summary>
        /// Liefert RestaurantBesitzer nach ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        IRestaurantBesitzer getRestaurantBesitzer(string ID);

    }
}
