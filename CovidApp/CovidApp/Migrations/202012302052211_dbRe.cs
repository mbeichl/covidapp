﻿namespace CovidApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dbRe : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Besuches",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        anfang = c.DateTime(nullable: false),
                        ende = c.DateTime(nullable: false),
                        BedienungID = c.Int(nullable: false),
                        deinemuddi = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Besuchers",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Vorname = c.String(),
                        Nachname = c.String(),
                        Geburtsdatum = c.String(),
                        CovidStatus = c.Boolean(nullable: false),
                        Email = c.String(),
                        PositveTestDate = c.DateTime(nullable: false),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Rappers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Restaurants",
                c => new
                    {
                        RestaurantID = c.Int(nullable: false),
                        Name = c.String(),
                        Adr_plz = c.Int(nullable: false),
                        Adr_strasse = c.String(),
                        Adr_ort = c.String(),
                        Küchenart = c.String(),
                        Mail = c.String(),
                        Telefonnummer = c.Int(nullable: false),
                        AnzahlMitarbeiter = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RestaurantID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Restaurants");
            DropTable("dbo.Rappers");
            DropTable("dbo.Besuchers");
            DropTable("dbo.Besuches");
        }
    }
}
