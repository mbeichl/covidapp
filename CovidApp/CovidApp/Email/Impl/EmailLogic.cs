﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Configuration;
using System.Net;

namespace CovidApp.Email.Impl
{
    public class EmailLogic : IEmailLogic
    {
        public string GetAlertText(String restauntName, string zeitraum)
        {
            //TODO: Email -  Alert Text schöner machen
            return String.Format("Da sie im Zeitraum {0} im Restaurant {1} waren, sind sie durch diverse Risikokontakte mit einer sehr hohen Wahrscheinlichkeit mit dem Covid-19 virus infiziert", zeitraum,restauntName);
        }

        public string GetPasswordText(String pw)
        {
            StringBuilder body = new StringBuilder();
            body.Append(@"<html>
                      <body>
                      <p>Lieber Covid-App User</p>
                      <p>Ihr neues Passwort lautet: </p>
                      
                     ");
            body.Append(pw);
            body.Append(@"
                         <p> Bitte diesmal nicht verlieren, </p>
                        <p><br />&nbsp; &nbsp; &nbsp;- <strong>Covid App Manager Bernard</strong></p>
                      </body>
                      </html>
                     ");
            return body.ToString();
        }

        /// <summary>
        /// Schickt eine Email an genau 1 Person
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="receiver">Empfänger</param>
        /// <param name="subject">Subject</param>
        /// <param name="text">Email-Body. HTML?</param>
        public void SendEmail(string to, string subject, string text)
        {
            ///Holt sich eine Emailadresse und einen Namen aus App.config
            MailAddress senderMailAddress = new MailAddress(ConfigurationManager.AppSettings.Get("SenderAddress"), ConfigurationManager.AppSettings.Get("SenderName"));

            MailMessage msg = new MailMessage();

            msg.To.Add(to);
            msg.From = senderMailAddress;
            msg.Subject = subject;
            msg.Body = text;
            msg.IsBodyHtml = true;
            abschicken(msg);
        }
        //Überladen mit einer Liste an Empfängern, statt einem Einzelnen
        public void SendEmail(List<String> to, string subject, string text)
        {
            MailAddress senderMailAddress = new MailAddress(ConfigurationManager.AppSettings.Get("SenderAddress"), ConfigurationManager.AppSettings.Get("SenderName"));

            MailMessage msg = new MailMessage();

            foreach(string receiver in to)
            {
                msg.To.Add(receiver);
            }
            msg.From = senderMailAddress;
            msg.Subject = subject;
            msg.Body = text;
            msg.IsBodyHtml = true;
            abschicken(msg);
        }

        public void abschicken(MailMessage msg)
        {
            SmtpClient client = new SmtpClient();
            try
            {
                client.Send(msg);
                Logger.log.Info("Email abgeschickt an: " + msg.To.ToString());
            }
            catch (Exception ex)
            {
                Logger.log.Error("Email send SMPT failed "+ ex.Message);
            }
        }
    }
}
