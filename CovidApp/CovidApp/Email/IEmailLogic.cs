﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace CovidApp.Email
{
    public interface IEmailLogic
    {
        /// <summary>
        /// Schickt eine Email
        /// </summary>
        /// <param name="receiver"></param>
        /// <param name="subject"></param>
        /// <param name="text"></param>
        void SendEmail(string receiver, string subject, string text);
        /// <summary>
        /// Überladen - Schickt eine Email an mehrere Personen
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="text"></param>
        void SendEmail(List<String> to, string subject, string text);

        /// <summary>
        /// Baut einen passenden Text zum Abschicken
        /// Fall: Coronavirus positiv gefunden
        /// </summary>
        /// <param name="restauntName"></param>
        /// <param name="zeitraum"></param>
        /// <returns></returns>
        String GetAlertText(String restauntName, String zeitraum);

        /// <summary>
        /// Baut einen passenden Text zum Abschicken
        /// Fall: Passwort vergessen
        /// </summary>
        /// <param name="pw"></param>
        /// <returns></returns>
        String GetPasswordText(string pw);
        

    }
}
