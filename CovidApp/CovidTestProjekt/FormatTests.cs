﻿using CovidApp.BesuchVerwaltung.Dt;
using CovidApp.BesuchVerwaltung.Et.Impl;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidTestProjekt
{
    [TestClass]
    public class FormatTests
    {
        Zeitspanne wide;
        Zeitspanne thin;
        Zeitspanne middle;
        DateTime d1;
        DateTime d3;
        DateTime d2;
        Besuch b1;
        Besuch b2;

        [TestCleanup]
        public void testClean()
        {
            d1 = d2 = d3 = DateTime.MinValue;
            wide = thin = null;
            b1 = b2 = null;
        }
        [TestInitialize]
        public void testInit()
        {
            d1 = new DateTime(2020, 10, 20, 15, 30, 30);
            d2 = new DateTime(2021, 01, 01, 15, 30, 30);
            d3 = new DateTime(2021, 01, 16, 15, 30, 30);
            wide = new Zeitspanne(d1, d3);
            thin = new Zeitspanne(d1, d2);
            middle = new Zeitspanne(d2, d3);
        }


        [TestMethod]
        public void TestZeitSpanneContainsTime()
        {
            Assert.IsTrue(wide.containsTime(d2));
        }
        [TestMethod]
        public void TestZeitSpanneOverlap()
        {
            var snapshot = new Zeitspanne(d2, d2);
            Assert.IsTrue(wide.isOverlap(snapshot));
            Assert.IsTrue(wide.isOverlap(thin));
            Assert.IsTrue(middle.isOverlap(thin));
            Assert.IsTrue(middle.isOverlap(wide));
        }
        [TestMethod]
        public void TestBesuchOutdated()
        {
            b1 = new Besuch() { anfang = d1, ende = d3 };
            b2 = new Besuch() { anfang = d1, ende = d2 };
            Assert.IsFalse(b1.IsOutdated());
            Assert.IsTrue(b2.IsOutdated());
        }

        [TestMethod]
        public void TestRestaurantRegEx()
        {
            Restaurant r = new Restaurant() { Adresse = "123456 - 923493629, 98765 .... Straßenname, PLZ: 332;\n 1" };
            Assert.AreEqual("98765",r.getPLZ());
        }
    }
    
}
