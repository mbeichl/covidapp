﻿using CovidApp.BesuchVerwaltung;
using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Et.Impl;
using CovidApp.BesuchVerwaltung.Persistence;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace CovidTestProjekt
{
    [TestClass]
    public class IBesuchAdminUCTest //Tests für alle UC von IBesuchAdmin
    {
        IBesuchPersistence besuchPers;
        IBesucherPersistence besucherPers;
        IRestaurantPersistence restaurantPers;
        IRestaurantBesitzerPersistence besitzerPers;
        IRestaurantBesitzerPersistence restaurantBesitzerPersistence;

        BesuchVerwaltungFactory facotry;
        IVerwaltungAdmin admin;

        IBesucher Levi;
        IBesucher Erwin;
        IBesucher Eren;
        IBesucher Mikasa;
        IBesucher Jean;
        IBesucher Armin;
        IBesucher Sasha;
        IBesucher Conny;
        IBesucher Jon;
        IBesucher Reiner;
        IBesucher Berthold;
        IBesucher Annie;
        IBesucher Zeke;


        RestaurantBesitzer restaurantbesitzer1;

        Restaurant restaurant1;
        Restaurant kebabo;

        IBesucher besucher1;
        IBesucher besucher2;
        IBesucher besucher3;
        IBesucher besucher4;

       IBesuch besuch1;
        IBesuch    besuch2;
        IBesuch besuch3;
        IBesuch besuch4;
        IBesuch besuch5;
        IBesuch besuch6;
        IBesuch besuch7;
        IBesuch besuch8;
        IBesuch besuch9;
        IBesuch besuch10;
        IBesuch besuch11;
        IBesuch besuch12;
        IBesuch besuch13;
        IBesuch besuch14;
        IBesuch besuch15;
        IBesuch besuch16;
        IBesuch besuch17;






        [TestInitialize]
        public void TestInitialize()
        {
            CovidApp.Logger.log = LogManager.GetLogger("APP_log");
            CovidApp.Logger.StartLogging();

            besuchPers = new BesuchPersistenceEF();
            besucherPers = new BesucherPersistenceEF();
            restaurantPers = new RestaurantPersistenceEF();
            besitzerPers = new RestaurantBesitzerPersistenceEF();
            restaurantBesitzerPersistence = new RestaurantBesitzerPersistenceEF();

            facotry = new BesuchVerwaltungFactory(besuchPers, restaurantPers, besucherPers, restaurantBesitzerPersistence);
            admin = facotry.getBesuchAdmin();

            #region delete
            besuchPers.DeleteAlleBesuche();
            //besucherPers.DeleteAlleBesucher();


            restaurantPers.DeleteAllRestaurants();
            besitzerPers.DeleteBesitzer("yoshimura@muenchen-mail.de");
            besitzerPers.DeleteBesitzer("mbeichl@muenchen-mail.de");

            #endregion delete

            RestaurantBesitzer rb = new RestaurantBesitzer("mbeichl@muenchen-mail.de", "Emil-Geis-Straße 17, 81379 München", "kek", "1234556677");
            admin.AddRestaurantBesitzer(rb);
            Restaurant r1 = new Restaurant("Antik", "Emil-Geis-Straße, 81379 München", "deinemuddi@gmx.de", "33344456");
            admin.AddRestaurantToBesitzer("mbeichl@muenchen-mail.de", r1);

            kebabo = new Restaurant("Kebabo", "Hansastraße 13, 81379 München", "kebabo@gmx.de", "53262165");
            admin.AddRestaurantToBesitzer("mbeichl@muenchen-mail.de", kebabo);

          

            restaurant1 = (Restaurant)admin.DictionaryRestaurants()[r1.RestaurantID];



            Levi = new Besucher("Levi", "Captain", "levi@gmx.de");
            Erwin = new Besucher("Erwin", "Smith", "erwin@gmx.de");
            Eren = new Besucher("Eren", "Yeager", "eren@gmx.de");
            Mikasa = new Besucher("Mikasa", "Ackermann", "mikasa@gmx.de");
            Jean = new Besucher("Jean", "Kirschstein", "jean@gmx.de");
            Armin = new Besucher("Armin", "Alert", "Armin@gmx.de");
            Conny = new Besucher("Conny", "Springer", "conny@gmx.de");
            Sasha = new Besucher("Sasha", "Braus", "sasha@gmx.de");
            Jon = new Besucher("Jon", "Snow", "jon@gmx.de");
            Reiner = new Besucher("Reiner", "Braun", "reiner@gmx.de");
            Berthold = new Besucher("Bertold", "Huber", "bertold@gmx.de");
            Annie = new Besucher("Annie", "Leonhardt", "annie@gmx.de");
            Zeke = new Besucher("Zeke", "Monke", "zeke@gmx.de");


            besucherPers.CreateBesucher(Levi);
            besucherPers.CreateBesucher(Erwin);
            besucherPers.CreateBesucher(Eren);
            besucherPers.CreateBesucher(Mikasa);
            besucherPers.CreateBesucher(Jean);
            besucherPers.CreateBesucher(Armin);
            besucherPers.CreateBesucher(Conny);
            besucherPers.CreateBesucher(Sasha);
            besucherPers.CreateBesucher(Jon);
            besucherPers.CreateBesucher(Reiner);
            besucherPers.CreateBesucher(Berthold);
            besucherPers.CreateBesucher(Annie);
            besucherPers.CreateBesucher(Zeke);



            besuch1 = admin.BesuchAnlegen(Eren, r1, new DateTime(2021, 01, 10, 10, 00, 00), new DateTime(2021, 01, 10, 12, 00, 00));
            besuch2 = admin.BesuchAnlegen(Erwin, r1, new DateTime(2021, 01, 10, 11, 00, 00), new DateTime(2021, 01, 10, 13, 00, 00));
            besuch3 = admin.BesuchAnlegen(Conny, r1, new DateTime(2021, 01, 10, 07, 00, 00), new DateTime(2021, 01, 10, 08, 30, 00));
            besuch14 = admin.BesuchAnlegen(Reiner, r1, new DateTime(2021, 01, 10, 06, 00, 00), new DateTime(2021, 01, 10, 07, 30, 00));
            besuch15 = admin.BesuchAnlegen(Berthold, r1, new DateTime(2021, 01, 10, 08, 00, 00), new DateTime(2021, 01, 10, 10, 30, 00));
            besuch16 = admin.BesuchAnlegen(Annie, r1, new DateTime(2021, 01, 10, 06, 30, 00), new DateTime(2021, 01, 10, 07, 30, 00));
            besuch17 = admin.BesuchAnlegen(Zeke, kebabo, new DateTime(2021, 01, 10, 18, 15, 00), new DateTime(2021, 01, 10, 20, 30, 00));


            besuch4 = admin.BesuchAnlegen(Levi, r1, new DateTime(2021, 01, 10, 10, 00, 00), new DateTime(2021, 01, 10, 15, 00, 00));
            besuch5 = admin.BesuchAnlegen(Mikasa, kebabo, new DateTime(2021, 01, 10, 08, 00, 00), new DateTime(2021, 01, 10, 09, 00, 00));
            besuch6 = admin.BesuchAnlegen(Jean, kebabo, new DateTime(2021, 01, 10, 07, 00, 00), new DateTime(2021, 01, 10, 08, 30, 00));
            besuch7 = admin.BesuchAnlegen(Armin, kebabo, new DateTime(2021, 01, 10, 10, 00, 00), new DateTime(2021, 01, 10, 16, 30, 00));
            
            
            besuch8 = admin.BesuchAnlegen(Jon, kebabo, new DateTime(2021, 01, 07, 14, 00, 00), new DateTime(2021, 01, 07, 15, 00, 00));



            besuch9 = admin.BesuchAnlegen(Sasha, kebabo, new DateTime(2021, 01, 10, 16, 00, 00), new DateTime(2021, 01, 10, 18, 00, 00));

            besuch10 = admin.BesuchAnlegen(Jon, kebabo, new DateTime(2021, 01, 10, 16, 00, 00), new DateTime(2021, 01, 10, 18, 00, 00));
            besuch11 = admin.BesuchAnlegen(Levi, kebabo, new DateTime(2021, 01, 10, 17, 00, 00), new DateTime(2021, 01, 10, 18, 00, 00));



            //veraltete Besuche
            besuch12 = admin.BesuchAnlegen(Armin, kebabo, new DateTime(2020, 12, 20, 10, 00, 00), new DateTime(2020, 12, 20, 15, 00, 00));
            besuch13 = admin.BesuchAnlegen(Mikasa, kebabo, new DateTime(2020, 12, 20, 10, 00, 00), new DateTime(2020, 12, 20, 15, 00, 00));





            
        }


        [TestMethod]
        public void CovidFallMelden()
        {






            admin.meldePositiv(Levi.BesucherID, new DateTime(2021, 01, 10));

            Assert.IsTrue(besucherPers.LoadBesucher()[Levi.BesucherID].CovidStatus);
            Assert.AreEqual(new DateTime(2021, 01, 10), besucherPers.LoadBesucher()[Levi.BesucherID].PositiveTestDate);

        }


        [TestMethod]
        public void Test14TageDeleteButton()
        {

           admin.Delete14TageAlteBesuche();


            IDictionary<int, IBesuch> besuche = admin.DictionaryBesuche();

            Assert.IsFalse(besuche.ContainsKey(besuch12.BesuchID));
            Assert.IsFalse(besuche.ContainsKey(besuch13.BesuchID));

            Assert.AreEqual(besuch11.BesuchID, besuche[besuch11.BesuchID].BesuchID);
            Assert.AreEqual(besuch9.BesuchID, besuche[besuch9.BesuchID].BesuchID);
            Assert.AreEqual(besuch4.BesuchID, besuche[besuch4.BesuchID].BesuchID);

        }

        [TestMethod]

        public void BesuchVergleichTest()
        {
          

              admin.meldePositiv(Jon.BesucherID, new DateTime(2021, 01, 08));



              admin.AlleBesuchDatenVergleichen();

            IDictionary<int, IBesucher> besucher = admin.DictionaryBesucher();

              Assert.IsTrue(besucher[Sasha.BesucherID].CovidStatus);
              Assert.IsTrue(besucher[Levi.BesucherID].CovidStatus);
            Assert.IsTrue(besucher[Armin.BesucherID].CovidStatus);

            Assert.IsFalse(besucher[Reiner.BesucherID].CovidStatus);
            Assert.IsFalse(besucher[Conny.BesucherID].CovidStatus);
              Assert.IsFalse(besucher[Jean.BesucherID].CovidStatus);
            Assert.IsFalse(besucher[Mikasa.BesucherID].CovidStatus);




        }
    }
}
