﻿using CovidApp;
using CovidApp.AccountVerwaltung;
using CovidApp.AccountVerwaltung.Logic;
using CovidApp.AccountVerwaltung.Persistence;
using CovidApp.AccountVerwaltung.Util;
using CovidApp.AccountVerwaltung.Util.Impl;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidTestProjekt
{
    [TestClass]
    public class TestAccountVerwaltung
    {
        private UserAccount u1, u2, u3, u4;
        private AccountVerwaltungFactory fac;
        private IAccountPersistence persistence;
        private IAccountAdmin admin;
        private IAccountModule module;
        Logger log;

        [TestInitialize]
        public void TestInitialize()
        {
            persistence = new AccountPersistence();
            fac = new AccountVerwaltungFactory(persistence);
            admin = fac.getAccountAdmin();
            module = fac.getAccountModule();
            CovidApp.Logger.log = LogManager.GetLogger("APP_log");
            CovidApp.Logger.StartLogging();


            u1 = new UserAccount("pwd11", "u1@gmail.com", "User1", "u1");
            u2 = new UserAccount("pwd22", "u2@gmail.com", "User2", "u2");
            u3 = new UserAccount("pwd33", "u3@gmail.com", "User3", "u3");
            u4 = new UserAccount("pwd44", "u4@gmail.com", "User4", "u4");


            persistence.CreateAccount(u1);
            persistence.CreateAccount(u2);
            persistence.CreateAccount(u3);
            persistence.CreateAccount(u4);
            admin.Refresh();

        }

        [TestMethod]
        public void TestChangePassword()
        {
            
            admin.ChangePassword(u1.userID, "neuesPasswort");
            Assert.AreEqual(persistence.LoadAccounts()[u1.userID].password, "neuesPasswort");      

        }

        [TestMethod]
        public void TestChangeUserAccount()
        {
            
            admin.ChangeUserAccount(u1.userID, "User1@gmail.com", "User1", "u1",u1.level);
            Assert.AreEqual(persistence.LoadAccounts()[u1.userID].email, "User1@gmail.com");        

        }
        
        [TestMethod]
        public void TestCreateCreateUserAccount()
        {
            
            UserAccount u5 = admin.CreateUserAccount("pwd55", "u5@gmail.com", "User5", "u5");
            Assert.AreEqual(persistence.LoadAccounts()[u5.userID].password,"pwd55");
            persistence.DeleteAccount(u5.userID);


        }

        [TestMethod]
        public void TestDeleteUserAccount()
        {
            
            admin.DeleteUserAccount(u1.userID);
            admin.Refresh();

            //wenn ich auf email von u1 zugreifen will
            //muss KeyNotFoundException geworfen werden
            //weil u1 nicht mehr in der DB ist
            try
            {
                string s = persistence.LoadAccounts()[u1.userID].email;
            }
            catch
            {
                Assert.IsTrue(true);
            }

           
        }

        [TestMethod]
        public void TestCheckDuplicate()
        {

            Assert.IsTrue(admin.CheckDuplicate(u3.email));

        }
      

        [TestCleanup]
        public void TearDown()
        {

            persistence.DeleteAccount(u1.userID);
            persistence.DeleteAccount(u2.userID);
            persistence.DeleteAccount(u3.userID);
            persistence.DeleteAccount(u4.userID);

        }

    }
}
