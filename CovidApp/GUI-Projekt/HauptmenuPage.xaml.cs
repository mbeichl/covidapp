﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using CovidApp.BesuchVerwaltung;
using CovidApp.BesuchVerwaltung.Dt;
using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Et.Impl;

namespace GUI_Projekt
{
    /// <summary>
    /// Interaction logic for Hauptmenue.xaml
    /// </summary>
    public partial class HauptmenuPage : Page
    {
        private IBesucher ident;
        
        public HauptmenuPage()
        {
            MainWindow.log.Info("Hauptmenü wird initialisiert");
            InitializeComponent();
            this.ident = (Besucher)Application.Current.Properties["CurrentUser"];
            Console.Out.WriteLine(ident.ToString());
        }
        public string Willkommen {
            get
            {
                MainWindow.log.Info("Willkommens Message geschrieben");
                return "Willkommen " + ident.ToString() +"!";
            }
         }



        private void Button_BesuchMelden_Click(object sender, RoutedEventArgs e)
        {
            // checkt ob bei allen etwas drinnen steht
            if (Box_RestaurantName.Text == "" || Box_DatePicker.SelectedDate == null || Box_ZeitraumBis.Text == "" || Box_ZeitraumVon.Text == "")
            {
                AlertEingabe.IsOpen = true;
                AlertEingabe.Visibility = System.Windows.Visibility.Visible;

                //timer für Popup
                DispatcherTimer time = new DispatcherTimer();
                time.Interval = TimeSpan.FromSeconds(2);
                time.Start();
                time.Tick += delegate
                {
                    AlertEingabe.IsOpen = false;
                    time.Stop();
                };
            }
            else
            {

                AlertEingabe.IsOpen = false;
                MainWindow.log.Info("Besuch Melden Click");
                IBesuchVerwaltungAdmin admin = (IBesuchVerwaltungAdmin)Application.Current.Properties["IBesuchAdmin"];
                IBesuchVerwaltungModule module = (IBesuchVerwaltungModule)Application.Current.Properties["IBesuchModule"];

                IBesucher besucher = (Besucher)Application.Current.Properties["Besucher"];
            

            string restaurantname = this.Box_RestaurantName.Text;

            //TODO : Einzelnes Restaurant raussuchen
            int ListSize = module.getRestaurant(restaurantname).Count;
            if (ListSize > 1)
            {
                //do something
            } else if (ListSize == 1)
            {
                IRestaurant restaurant = module.getRestaurant(restaurantname).First();
            } else
            {
                //Error Popup
            }

            #region ZeitSpanne zusammenbauen
            DateTime datum = (DateTime)this.Box_DatePicker.SelectedDate;

            if (!TimeSpan.TryParse(this.Box_ZeitraumVon.Text, out TimeSpan von))
            {
                //TODO: Error handling Zeitraum
                //boolean proceed = false; ??
                MessageBox.Show("Error");
            }
            DateTime datumVon = datum.Add(von);
            if (!TimeSpan.TryParse(this.Box_ZeitraumVon.Text, out TimeSpan bis))
            {
                MessageBox.Show("Error");
            }
            DateTime datumBis = datum.Add(bis);
            Zeitspanne zeitspanne = new Zeitspanne(datumVon, datumBis);
            #endregion ZeitSpanne zusammenbauen

            //Besucher test
            IRestaurantBesitzer Yoshimura = new Besitzer("Y");
            IBesucher Ken = new Gast("Ken", "Kaneki", "13.11.2000", "mbeichl@muenchen - mail.de");
            IRestaurant res = new Restaurant("Antik", new Adresse(81379, "Albrechtstraße", "Tokyo"), "cafe", "mbeichl@muenchen - mail.de", 123456789, 10, Yoshimura);
            IKellner tokachan = new Kellner("Toka", "Kaneki", "15.09.1997", "mbeichl@muenchen - mail.de", res);

            //admin.BesuchHinzufügen(new Besuch(besucher, res, zeitspanne, tokachan));
            }
        }

        private void Button_BesucheAnzeigen_Click(object sender, RoutedEventArgs e)
        {
            
            MainWindow.log.Info("Besuch Anzeigen Click");
            ListBesuchePage listeBesuche = new ListBesuchePage();
            this.NavigationService.Navigate(listeBesuche);
        }

        private void Button_CovidMelden_Click(object sender, RoutedEventArgs e)
        {

            MainWindow.log.Info("Covid Melden Click");
            MeldenCovidPage meldenCovid = new MeldenCovidPage();
            this.NavigationService.Navigate(meldenCovid);
        }
        
    }
}
