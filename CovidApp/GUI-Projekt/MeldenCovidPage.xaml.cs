﻿using CovidApp.AccountVerwaltung.Util;
using CovidApp.BesuchVerwaltung;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GUI_Projekt
{
    /// <summary>
    /// Interaction logic for MeldenCovidPage.xaml
    /// </summary>
    public partial class MeldenCovidPage : Page
    {
        public MeldenCovidPage()
        {
            InitializeComponent();
        }

        private void Fall_Melden_Click(object sender, RoutedEventArgs e)
        {
           
            
           // IAcount acount = Application.Current.Properties.Add("IBesuchModule", besuchModule); */

IBesuchVerwaltungAdmin admin = (IBesuchVerwaltungAdmin)Application.Current.Properties["IBesuchAdmin"];
           IUserAccount user = (IUserAccount)Application.Current.Properties["IUserAccount"]; //NOT YET IMPLEMENTED IN MAIN WINDOW
            //admin.updateGast("hanspeter", "mbeichl@muenchen - mail.de", "1111111", true);
            if (TestDate.DisplayDate.Subtract(DateTime.Now).TotalDays < 14.0)
            {
                admin.meldePositiv(user.userID, TestDate.DisplayDate);
            }
            else
            {
                Console.WriteLine("Test älter als zwei Wochen =>Unbedeuted, da ´keine Ansteckungsgefahr mehr besteht");
            }
        }
    }
}
