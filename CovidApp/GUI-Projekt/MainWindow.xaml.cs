﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CovidApp.AccountVerwaltung.Logic;
using CovidApp.BesuchVerwaltung;
using CovidApp.BesuchVerwaltung.Persistence;
using CovidApp.BesuchVerwaltung.Et.Impl;
using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Uc;
using CovidApp.BesuchVerwaltung.Uc.Impl;
using CovidApp.AccountVerwaltung.Logic.Impl;
using log4net;
using log4net.Config;
using CovidApp.AccountVerwaltung.Persistence;
using CovidApp.AccountVerwaltung;
using CovidApp;
using CovidApp.Email;

namespace GUI_Projekt
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : NavigationWindow
    {
        private IVerwaltungAdmin besuchAdmin;
        private IVerwaltungModule besuchModule;
        private IAccountAdmin accountAdmin;
        private IAccountModule accountModule;
        public static readonly ILog log = LogManager.GetLogger("GUI_log");

        public MainWindow()
        {
            #region Logger Start
            XmlConfigurator.Configure();
            log.Info("Start Logging");

            CovidApp.Logger.log = LogManager.GetLogger("APP_log");
            CovidApp.Logger.StartLogging();
            #endregion

            InitializeComponent();

            //Erzeuge Account Logik
            IAccountPersistence persistenceAccount = new AccountPersistence();

            AccountVerwaltungFactory factory = new AccountVerwaltungFactory(persistenceAccount);
            accountAdmin = factory.getAccountAdmin();
            accountModule = factory.getAccountModule();

            Application.Current.Properties.Add("IAccountAdmin", accountAdmin);   
            Application.Current.Properties.Add("IAccountModule", accountModule);

          


            //Erzeuge Besuch Verwaltung Logik
            IBesuchPersistence besuchPers = new BesuchPersistenceEF();
            IRestaurantPersistence restaurantPers = new RestaurantPersistenceEF();
            IBesucherPersistence besucherPers = new BesucherPersistenceEF();
            IRestaurantBesitzerPersistence besitzerPers = new RestaurantBesitzerPersistenceEF();

            BesuchVerwaltungFactory fac = new BesuchVerwaltungFactory(besuchPers, restaurantPers, besucherPers, besitzerPers);
            besuchAdmin = fac.getBesuchAdmin();
            besuchModule = fac.getVerwaltungModule();


            Application.Current.Properties.Add("IVerwaltungAdmin", besuchAdmin);
            Application.Current.Properties.Add("IVerwaltungModule", besuchModule);


            EmailFactory emailFactory = new EmailFactory();
            IEmailLogic emailLogic = emailFactory.GetEmailLogic();

            Application.Current.Properties.Add("IEmailLogic", emailLogic);

        }
    }
}
