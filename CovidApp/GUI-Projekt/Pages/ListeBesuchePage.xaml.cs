﻿using CovidApp.BesuchVerwaltung;
using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Et.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GUI_Projekt
{
    /// <summary>
    /// Interaction logic for ListeBesuchePage.xaml
    /// </summary>
    public partial class ListBesuchePage : Page
    {
        private IBesucher ident;
        IVerwaltungAdmin admin;
        IVerwaltungModule module;
        public ListBesuchePage()
        {
            InitializeComponent();
            MainWindow.log.Info("ListeBesuchePage wird initialisiert");
            this.ident = (Besucher)Application.Current.Properties["CurrentUser"];
            this.admin = (IVerwaltungAdmin)Application.Current.Properties["IVerwaltungAdmin"];
            this.module = (IVerwaltungModule)Application.Current.Properties["IVerwaltungModule"];

            var displayList = module.getBesucheFromBesucher(this.ident.BesucherID);


            DataGrid_Besuche.ItemsSource = displayList.Select(o => new
            { Datum = ((DateTime)o.anfang).ToString("dd.MM.yy"), 
                Anfang = ((DateTime)o.anfang).ToString("HH:mm"), 
                Ende = ((DateTime)o.ende).ToString("HH:mm"), 
                Restaurant = o.Restaurant.ToString(),
                Addresse = o.Restaurant.Adresse
            }).ToList();
        }
    }
}
