﻿using CovidApp.BesuchVerwaltung;
using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Et.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GUI_Projekt
{
    /// <summary>
    /// Interaction logic for BesitzerMenuPage.xaml
    /// </summary>
    public partial class BesitzerMenuPage : Page
    {
        List<IRestaurant> restaurantListe;
        IRestaurantBesitzer ident;
        IVerwaltungModule module;
        IVerwaltungAdmin admin;
        public BesitzerMenuPage()
        {
            InitializeComponent();
            this.ident = (RestaurantBesitzer)Application.Current.Properties["CurrentUser"];
            this.module = (IVerwaltungModule)Application.Current.Properties["IVerwaltungModule"];
            this.admin = (IVerwaltungAdmin)Application.Current.Properties["IVerwaltungAdmin"];

            //Falls die Identität nicht stimmt, zurückschicken an Login
            this.Loaded += (sender, e) =>
            {
                if (ident == null)
                {
                    LoginPage login = new LoginPage();
                    NavigationService.Navigate(login);
                }
            };
            restaurantListe = module.getRestaurantFromBesitzer(ident.email);
            ComboBox_Select.ItemsSource = restaurantListe;
        }

        private void Button_BesucheAnlegen_Click(object sender, RoutedEventArgs e)
        {
            Restaurant selected = (Restaurant)ComboBox_Select.SelectedItem;
            try
            {
                var besuche = module.ReadBesucheFromFile(TextBox_FilePath.Text);
                foreach (var b in besuche)
                {
                   admin.AddBesuchToRestaurant(selected.RestaurantID, (Besuch)b);
                }
                MessageBox.Show("Besuche angelegt! Bitte nicht mehrmals drücken");
            }
            catch
            {
                MessageBox.Show("Bitte Pfad auswählen");
            }            
        }


        private void Button_RestaurantAnlegen_Click(object sender, RoutedEventArgs e)
        {
            
            MainWindow.log.Info("Restaurant Anlegen Button gedrückt");

            string c = "";
            //TODO mehr checks?
            if (TextBox_Name.Text == "" || TextBox_Adresse.Text == "" || TextBox_Mail.Text == "" || TextBox_Tel.Text == "") c = "TextEmpty";
            if (restaurantListe.Any(a => a.Adresse == TextBox_Adresse.Text)) c = "Duplikat";
            else c = "Restaurant";

            switch (c)
            {
                case "TextEmpty":
                    MessageBox.Show("Keine Daten eingegeben", "Achtung", MessageBoxButton.OK, MessageBoxImage.Warning);
                    break;
                case "Duplikat":
                    MessageBox.Show("Restaurant gibt es schon!", "Achtung", MessageBoxButton.OK, MessageBoxImage.Warning);
                    break;
                case "Restaurant":
                    {
                    Restaurant newRestaurant = new Restaurant { Adresse = TextBox_Adresse.Text, Mail = TextBox_Mail.Text, Telefonnummer = TextBox_Tel.Text, Name = TextBox_Name.Text };
                    restaurantAnlegen(newRestaurant);                    
                    //refresh liste
                    restaurantListe = module.getRestaurantFromBesitzer(ident.email);
                    ComboBox_Select.ItemsSource = restaurantListe;
                    break;
                    }
            }


        }

        private void restaurantAnlegen(Restaurant newRestaurant)
        {
            try
            {
                admin.AddRestaurantToBesitzer(ident.email, newRestaurant);
                MessageBox.Show("Restaurant erfolgreich angelegt! \n: " + newRestaurant.ToString());
            }
            catch (Exception ex)
            {
                MainWindow.log.Error("Anlegen gescheitert: " + ex.Message);

                MessageBox.Show("Bitte mit gültigen Besitzeraccount einloggen: " + ex.Message);
            }
        }
        private void TextBox_FilePath_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog fileDialog = new Microsoft.Win32.OpenFileDialog();

            fileDialog.DefaultExt = ".txt";
            fileDialog.Filter = "txt Dateien (*.txt)|*.txt";

            bool? result = fileDialog.ShowDialog();

            if (result == true)
            {
                string filename = fileDialog.FileName;
                TextBox_FilePath.Text = filename;
            }
        }
    }
}
