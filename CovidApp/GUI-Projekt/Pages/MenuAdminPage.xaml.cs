﻿using CovidApp.AccountVerwaltung;
using CovidApp.AccountVerwaltung.Logic;
using CovidApp.AccountVerwaltung.Persistence;
using CovidApp.AccountVerwaltung.Util;
using CovidApp.BesuchVerwaltung;
using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Et.Impl;
using CovidApp.BesuchVerwaltung.Uc.Impl;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GUI_Projekt
{
    /// <summary>
    /// Interaction logic for MenuAdminPage.xaml
    /// </summary>
    public partial class MenuAdminPage : Page
    {
        private IVerwaltungAdmin verwaltungAdmin;
        private IAdminHelper adminHelper;
        IAccountAdmin accAdmin;
        private IUserAccount userSelected;
        public MenuAdminPage()
        {
            InitializeComponent();
            IAccountPersistence persistence = new AccountPersistence();
            adminHelper = new AdminHelper(persistence);
            verwaltungAdmin = (IVerwaltungAdmin)Application.Current.Properties["IVerwaltungAdmin"];
            accAdmin = (IAccountAdmin)Application.Current.Properties["IAccountAdmin"];

            ComboBox_Level.ItemsSource = new List<string> { "User", "Besitzer", "Admin" };

        }





        private void TextBox_File_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog fileDialog = new Microsoft.Win32.OpenFileDialog();

            fileDialog.DefaultExt = ".csv";
            fileDialog.Filter = "csv Dateien (*.csv)|*.csv";

            bool? result = fileDialog.ShowDialog();

            if (result == true)
            {
                string filename = fileDialog.FileName;
                TextBox_File.Text = filename;
            }
        }

        

        private void Button_Read_Besitzer_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult mB = MessageBox.Show("Besitzer anlegen?", "Confirmation", MessageBoxButton.OKCancel, MessageBoxImage.Question);
            if (mB == MessageBoxResult.OK)
            {
                List<RestaurantBesitzer> rbList = new List<RestaurantBesitzer>();
                try
                {
                    rbList = adminHelper.ReadRestaurantBesitzerFromFile(TextBox_File.Text);
                    foreach (RestaurantBesitzer rb in rbList)
                    {
                        MainWindow.log.Info(rb.ToString() + " eingelesen");
                        verwaltungAdmin.AddRestaurantBesitzer(rb);
                    }
                    TextBlock_Meldung.Text = "Eingelesen";
                }
                catch (Exception ex)
                {
                    MainWindow.log.Info("Achtung: Text File nicht gelesen! " + ex);
                    MessageBoxResult warning = MessageBox.Show(ex.Message, "Confirmation", MessageBoxButton.OK, MessageBoxImage.Warning);
                }                                
            }
            else
            {
                TextBlock_Meldung.Text = "Abgebrochen";
            }            
        }

        private void Button_Read_Restaurant_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult mB = MessageBox.Show("Restaurants anlegen?", "Confirmation", MessageBoxButton.OKCancel, MessageBoxImage.Question);
            if (mB == MessageBoxResult.OK)
            {
                var rbList = new SortedDictionary<Restaurant, string>();
                try
                {
                    rbList = adminHelper.ReadRestaurantsFromFile(TextBox_File.Text);
                    foreach (var paar in rbList)  
                    {
                        try
                        {
                            MainWindow.log.Info(paar.Key.ToString() + " eingelesen");
                            verwaltungAdmin.AddRestaurantToBesitzer(paar.Value, paar.Key);
                        }
                        catch (Exception ex)

                        {
                            MainWindow.log.Info(ex.Message + " aufgetreten");
                        }
                        
                    }
                    TextBlock_Meldung.Text = "Eingelesen";
                }
                catch (Exception ex)
                {
                    MainWindow.log.Info("Achtung: Text File nicht gelesen! " + ex);
                    MessageBoxResult warning = MessageBox.Show(ex.Message, "Confirmation", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            else
            {
                TextBlock_Meldung.Text = "Abgebrochen";
            }
        }

        private void Button_Search_Click(object sender, RoutedEventArgs e)
        {
            if (Int32.TryParse(TextBox_UserInput.Text, out int res))
            {
                userSelected = adminHelper.getAccount(res);
            } else
            {
                userSelected = adminHelper.getAccount(TextBox_UserInput.Text);
            }

            if (userSelected != null)
            {
                TextBox_UserInput.Text = userSelected.ToString();
                TextBlock_Selected.Text = userSelected.userID.ToString();
            }
            else
                MessageBox.Show("Nicht gefunden");            

        }

        private void Button_SetLevel_Click(object sender, RoutedEventArgs e)
        {
            int level = 0;
            if (ComboBox_Level.SelectedItem==null) level = 0;
            else if (ComboBox_Level.SelectedItem.ToString() == "Besitzer") level = 1;
            else if (ComboBox_Level.SelectedItem.ToString() == "Admin") level = 2;
            else if (ComboBox_Level.SelectedItem.ToString() == "User") level = 0;

            if (userSelected == null || userSelected.userID == 0) { MessageBox.Show("Keinen User ausgewählt!"); return; }

            switch (level)
                {
                    case 1:
                    {

                            IRestaurantBesitzer rb = verwaltungAdmin.AddRestaurantBesitzer(
                                            new RestaurantBesitzer { name = (userSelected.ToString()), email = userSelected.email });

                            MainWindow.log.Info("Besitzer angelegt mit ID" + rb.email);

                            accAdmin.AddBesitzerID(userSelected.userID, rb.email);

                            adminHelper.elevateAccount(userSelected.userID, level);
                            MessageBox.Show(String.Format("user ID: {0} auf Level BESITZER geändert", userSelected.userID));
                            break;
                        }
                    case 2:
                        {
                            adminHelper.elevateAccount(userSelected.userID, level);
                            MessageBox.Show(String.Format("user ID: {0} auf Level ADMIN geändert", userSelected.userID));
                        }
                        break;
                    default:
                        {
                            adminHelper.elevateAccount(userSelected.userID, level);
                            MessageBox.Show(String.Format("user ID: {0} auf USER geändert", userSelected.userID));
                            break;
                        }
                    
            }
        }

        private void TextBox_DLPath_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

            var folderDialog = new System.Windows.Forms.FolderBrowserDialog();
            var result = folderDialog.ShowDialog();
            if(result==System.Windows.Forms.DialogResult.OK) TextBox_DLPath.Text = folderDialog.SelectedPath;
        }

        private void Button_Download_Click(object sender, RoutedEventArgs e)
        {
            if (TextBox_DLPath.Text != "")
            {
                adminHelper.downloadDB(TextBox_DLPath.Text);
                MessageBox.Show("Downloaded");
            } else MessageBox.Show("Bitte Pfad auswählen");
        }
    }
}

