﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using CovidApp.BesuchVerwaltung;
using CovidApp.BesuchVerwaltung.Dt;
using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Et.Impl;
using GUI_Projekt.Pages;

namespace GUI_Projekt
{
    /// <summary>
    /// Interaction logic for Hauptmenue.xaml
    /// </summary>
    public partial class HauptmenuPage : Page
    {
        private IBesucher ident;
        IVerwaltungAdmin admin;
        IVerwaltungModule module;
        Restaurant restaurant;
        DateTime? datumVon;
        DateTime? datumBis;

        public HauptmenuPage()
        {
            MainWindow.log.Info("Hauptmenü wird initialisiert");
            InitializeComponent();
            this.ident = (Besucher)Application.Current.Properties["CurrentUser"];
            this.admin = (IVerwaltungAdmin)Application.Current.Properties["IVerwaltungAdmin"];
            this.module = (IVerwaltungModule)Application.Current.Properties["IVerwaltungModule"];
            restaurant = new Restaurant();
        }
        public string Willkommen {
            get
            {
                try { 
                MainWindow.log.Info("Willkommens Message geschrieben");
                return "Willkommen " + ident.ToString() +"!";
                }
                catch (ArgumentNullException e)
                {
                    MainWindow.log.Info(e.Message);
                }
                return ("Identität nicht gefunden");
            }
         }

        private void Button_BesuchMelden_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.log.Info("Besuch Melden Click");
            Besucher besucher = (Besucher)this.ident;


            //Restaurant Checken
            string restaurantname = this.Box_RestaurantName.Text;
            var RestList = module.getRestaurant(restaurantname);

            //Datum checken
            DateTime? datum = this.Box_DatePicker.SelectedDate;
            datumVon = getDatumFromString(this.Box_ZeitraumVon.Text, datum);
            datumBis = getDatumFromString(this.Box_ZeitraumBis.Text, datum);
            string c = "";

            if (Box_RestaurantName.Text == "" || Box_ZeitraumBis.Text == "" || Box_ZeitraumVon.Text == "") c = "TextEmpty";
            else if (datum == null) c = "DateEmpty";
            else if (datumVon == null || datumBis == null) c = "IncorrectZeitFormat";
            else if (datumVon > datumBis) c = "Ankunft>Abreise";
            else if (RestList.Count < 1) c = "RestaurantNotFound";
            else if (RestList.Count > 1) c = "MultipleRestaurants";
            else if (RestList.Count == 1) c = "SingleRestaurant";

            switch (c)
            {
                case "TextEmpty":
                    MessageBox.Show("Keine Daten eingegeben", "Achtung", MessageBoxButton.OK, MessageBoxImage.Warning);
                    break;
                case "DateEmpty":
                    MessageBox.Show("Bitte Datum auswählen!", "Achtung", MessageBoxButton.OK, MessageBoxImage.Warning);
                    break;
                case "IncorrectZeitFormat":
                    MessageBox.Show("Bitte korrekte Zeit eingeben\nZeitraum Format: HH:MM", "Achtung", MessageBoxButton.OK, MessageBoxImage.Warning);
                    break;
                case "Ankunft>Abreise":
                    MessageBox.Show("Ankunfszeit später als Abreisezeit!", "Achtung", MessageBoxButton.OK, MessageBoxImage.Warning);
                    break;
                case "RestaurantNotFound":
                    MessageBox.Show("Restaurant: " + restaurantname + " nicht gefunden");
                    break;
                case "MultipleRestaurants":
                    MainPopup.IsOpen = true;
                    ComboBox_Restaurant.ItemsSource = RestList;
                    break;
                case "SingleRestaurant":
                    this.restaurant = (Restaurant)module.getRestaurant(restaurantname).First();
                    besuchAnlegen(restaurant, datumVon, datumBis);
                    break;
            }
            
        }

        private void besuchAnlegen(Restaurant restaurant, DateTime? datumVon, DateTime? datumBis)
        {
            try
            {
                admin.BesuchAnlegen((Besucher)this.ident, restaurant, (DateTime)datumVon, (DateTime)datumBis);
                MessageBox.Show(String.Format("Besuch erfolgreich angelegt!\nAm {0}, {1} - {2} bei {3}\n{4}", ((DateTime)datumVon).ToString("dd.MM.yy"), ((DateTime)datumVon).ToString("HH:mm"), ((DateTime)datumBis).ToString("HH:mm"), restaurant.Name, restaurant.Adresse));
            }
            catch (Exception ex)
            {
                MainWindow.log.Error("Anlegen gescheitert: " + ex.Message);
                MessageBox.Show("Anlegen gescheitert:\n" + ex.Message);
            }
        }
        

        private DateTime? getDatumFromString(string eingabe, DateTime? date)
        {
            DateTime dt;
            var deDE = new CultureInfo("de-DE");
            string[] formats = { "HH:mm", "HH-mm", "hh-mm", "hh mm", "HHmm", "h:mm", "h-mm" };

            if (!DateTime.TryParseExact(eingabe, formats, deDE , System.Globalization.DateTimeStyles.None, out dt))
            {               
                return null;
            } 
            TimeSpan time = dt.TimeOfDay;
            if (dt.TimeOfDay.TotalMinutes> 1440)
            {
                return null;
            }
            date = date + time;
            return date;
        }



        private void Button_BesucheAnzeigen_Click(object sender, RoutedEventArgs e)
        {
            
            MainWindow.log.Info("Besuch Anzeigen Click");
            ListBesuchePage listeBesuche = new ListBesuchePage();
            this.NavigationService.Navigate(listeBesuche);
        }

        private void Button_CovidMelden_Click(object sender, RoutedEventArgs e)
        {

            MainWindow.log.Info("Covid Melden Click");
            MeldenCovidPage meldenCovid = new MeldenCovidPage();
            this.NavigationService.Navigate(meldenCovid);
        }

        private void Button_PasswortÄndern_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.log.Info("Passwort Ändern Click");
            PasswortÄndern passwortÄndern = new PasswortÄndern();
            this.NavigationService.Navigate(passwortÄndern);
        }

        private void Button_Select_Click(object sender, RoutedEventArgs e)
        {
            this.restaurant = (Restaurant)ComboBox_Restaurant.SelectedItem;
            besuchAnlegen(restaurant, datumVon, datumBis);
        }
        private void OnNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(e.Uri.AbsoluteUri);
            e.Handled = true;
        }
    }
}
