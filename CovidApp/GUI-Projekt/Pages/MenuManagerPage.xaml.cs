﻿using CovidApp.BesuchVerwaltung;
using CovidApp.BesuchVerwaltung.Dt;
using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Et.Impl;
using CovidApp.BesuchVerwaltung.Persistence;
using CovidApp.Email;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GUI_Projekt
{
    /// <summary>
    /// Interaction logic for MenuManagerPage.xaml
    /// </summary>
    public partial class MenuManagerPage : Page
    {


        IVerwaltungAdmin admin = (IVerwaltungAdmin)Application.Current.Properties["IVerwaltungAdmin"];
        IRestaurantPersistence restaurantPers = (IRestaurantPersistence)Application.Current.Properties["IRestaurantPersistence"];
        List<BesuchManagerTabelle> vglTabelle;
        List<BesuchManagerTabelle> besucheTab;
        //neu 


        public MenuManagerPage()
        {
            InitializeComponent();

            //Aktualisieren
            admin.AlleBesuchDatenVergleichen();

            vglTabelle = new List<BesuchManagerTabelle>();


            //  MainWindow.log.Info("GefiltereBesuche Tabelle wird erstellt");

            besucheTab = new List<BesuchManagerTabelle>();
            
            foreach (IBesuch b in admin.ListBesuche())
            {
                BesuchManagerTabelle bmt = new BesuchManagerTabelle(b);
                besucheTab.Add(bmt);

            }
            BesucheGefiltert.ItemsSource = besucheTab;



            // neu?
            var displayList = admin.ListBesuche();

            /*
            BesucheGefiltert.ItemsSource = displayList.Select(o => new
            {
                Besucher = o.Besucher.ToString(),
                Datum = ((DateTime)o.anfang).ToString("dd.MM.yy"),
                Anfang = ((DateTime)o.anfang).ToString("HH:mm"),
                Ende = ((DateTime)o.ende).ToString("HH:mm"),
                Restaurant = o.Restaurant.ToString(),
                Addresse = o.Restaurant.getPLZ(),
                CovidStatus = o.Besucher.CovidStatus,
                BesuchID = o.BesuchID
            }).ToList();
            */

        }


        //Konsturktor erstellt aus einem IBesuch einen Besuch wie er in Der ManagerTabelle dargestellt werden soll
        public class BesuchManagerTabelle
        {
            IVerwaltungAdmin admin = (IVerwaltungAdmin)Application.Current.Properties["IVerwaltungAdmin"];
            public String name { get; set; }
            public String Zeitraum { get; set; }
            public String Restaurant { get; set; }
            public String plz { get; set; }
            public bool CovidStatusBesucher { get; set; }
            public int BesucherID { get; set; }
            public bool Bearbeitet { get; set; }


            public int BesuchID { get; set; }

            IRestaurantPersistence restaurantPers = (IRestaurantPersistence)Application.Current.Properties["IRestaurantPersistence"];



            public BesuchManagerTabelle(IBesuch b)
            {
                name = admin.DictionaryBesucher()[b.BesucherID].Vorname + " " + admin.DictionaryBesucher()[b.BesucherID].Nachname;

                //01.12.2020 10:00:00 - 01.12.2020 12:00:00
                String temp = b.anfang.ToString() + " - " + b.ende.ToString();

                String[] StrArr = temp.Split(' ');
               Zeitraum = StrArr[1] + " - " + StrArr[4] + ", " + StrArr[3] + " ";

                Restaurant = admin.DictionaryRestaurants()[b.RestaurantID].Name;                    //warum ist das null: b.Restaurant.RestaurantID ?


                CovidStatusBesucher = admin.DictionaryBesucher()[b.BesucherID].CovidStatus;   //warum ist das null: b.Restaurant.RestaurantID ?

               // Adresse adr = new Adresse(admin.DictionaryRestaurants()[b.RestaurantID].Adresse);
                plz = admin.DictionaryRestaurants()[b.RestaurantID].getPLZ();

                BesucherID = b.BesucherID;

                Bearbeitet = false;

                BesuchID = b.BesuchID;

            }
        }





        private void Dump_Click(object sender, RoutedEventArgs e)
        {
            IVerwaltungAdmin besAdmn = (IVerwaltungAdmin)Application.Current.Properties["IVerwaltungAdmin"];

            IBesuchPersistence besuchPers = (IBesuchPersistence)Application.Current.Properties["besuchPers"];




            if (besAdmn.ListBesuche() != null)
            {

                List<IBesuch> besuche = besAdmn.ListBesuche();



                foreach (Besuch b in besuche)
                {
                    //Rausfinden wie alt der der Besuch ist
                    DateTime today = DateTime.Today;
                    TimeSpan BesuchAlter = today - b.getDatum();
                    double BesuchAlterInTagen = BesuchAlter.TotalDays;
                    bool gabsDaten = false;
                    if (BesuchAlterInTagen > 14) //Löschen wenn älter als 14 Tage
                    {
                        gabsDaten = true;
                        besAdmn.DeleteBesuch(b.BesuchID);
                    }

                    if (gabsDaten)
                    {
                        this.Feedback.Text = "Erfolgreich  Daten älter als 14 Tage gelöscht";
                    }
                    else
                    {
                        this.Feedback.Text = "Gab keine Daten älter als 14 Tage";
                    }

                }
            }
            else
            {
                Feedback.Text = "FEHLER: besAdmn.ListBesuche() == null";

            }




            //Aktualisieren
            //admin.AlleBesuchDatenVergleichen();

        }



        private void Restaurant_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Aktualisieren
            //admin.AlleBesuchDatenVergleichen();


            List<BesuchManagerTabelle> BesucheTabelle = new List<BesuchManagerTabelle>();


            foreach (IBesuch b in admin.ListBesuche())
            {
                if ((admin.DictionaryRestaurants()[b.RestaurantID].Name).Equals(Restaurant.Text))
                {

                    BesucheTabelle.Add(new BesuchManagerTabelle(b));
                }

            }

            if (BesucheTabelle.Count != 0)
            {
                BesucheGefiltert.ItemsSource = BesucheTabelle;
            }
            else
            {
                BesucheGefiltert.ItemsSource = new List<BesuchManagerTabelle>();

            }


        }

        /* protected virtual void OnMouseDoubleClick(BesucheGefiltert.MouseDoubleClick)
         {
             BesucheGefiltert.MouseDoubleClick
                 DataGrid.MouseDoubleClickEvent

         }*/
        private void dataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            vglTabelle = new List<BesuchManagerTabelle>();
            if (sender != null)
            {
                DataGrid grid = sender as DataGrid;



                if (grid != null && grid.SelectedItems != null && grid.SelectedItems.Count == 1)
                {
                    //This is the code which helps to show the data when the row is double clicked.
                    DataGridRow dgr = grid.ItemContainerGenerator.ContainerFromItem(grid.SelectedItem) as DataGridRow;
                    BesuchManagerTabelle dr = (BesuchManagerTabelle)dgr.Item;

                    //String ProductDescription = dr[2].ToString();




                    List<IBesuch> besucheZeitgleich = admin.BesucheZurGleichenZeit(dr.BesuchID);  //mit Klick auf Zeile oder Hover wird ID ausgewäht
                    foreach (IBesuch b in besucheZeitgleich)
                    {
                        int besucherID_dr = admin.DictionaryBesuche()[dr.BesuchID].BesucherID;

                        if (b.BesuchID != dr.BesuchID && b.BesucherID != besucherID_dr && b.RestaurantID == admin.DictionaryRestaurants()[admin.DictionaryBesuche()[dr.BesuchID].RestaurantID].RestaurantID) //schaut ob gleiches Restaurant //schaut obs gleiche //eig. egal, außer jemand macht die Hermine 
                        {
                            BesuchManagerTabelle bmt = new BesuchManagerTabelle(b);
                            vglTabelle.Add(bmt);

                            int userVal;
                            if (int.TryParse(UserID.Text, out userVal))
                            {
                                UserID_ed.Text = userVal.ToString();
                                Nachname_ed.Text = admin.DictionaryBesucher()[userVal].Nachname;
                                Email_ed.Text = admin.DictionaryBesucher()[userVal].Email;
                                Vorname_ed.Text = admin.DictionaryBesucher()[userVal].Vorname;
                            }
                        }

                    }

                    SelberZeitraum.ItemsSource = vglTabelle;
                    //MessageBox.Show("You Clicked : \r\nName : " + ProductName + "\r\nDescription : " + ProductDescription);
                }
            }




        }




        private void AlleDatenClick(object sender, RoutedEventArgs e)
        {
            //Aktualisieren
           // admin.AlleBesuchDatenVergleichen();
            List<BesuchManagerTabelle> besucheTab = new List<BesuchManagerTabelle>();

            foreach (IBesuch b in admin.ListBesuche())
            {
                BesuchManagerTabelle bmt = new BesuchManagerTabelle(b);
                besucheTab.Add(bmt);

            }

            BesucheGefiltert.ItemsSource = besucheTab;

        }

        private void UserID_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Aktualisieren
            //admin.AlleBesuchDatenVergleichen();
            List<BesuchManagerTabelle> BesucheTabelle = new List<BesuchManagerTabelle>();

            foreach (IBesuch b in admin.ListBesuche())
            {
                int userVal;
                if (int.TryParse(UserID.Text, out userVal))
                {
                    if (b.BesucherID == userVal)
                    {

                        BesucheTabelle.Add(new BesuchManagerTabelle(b));
                        UserID_ed.Text = userVal.ToString();
                        Nachname_ed.Text = admin.DictionaryBesucher()[userVal].Nachname;
                        Email_ed.Text = admin.DictionaryBesucher()[userVal].Email;
                        Vorname_ed.Text = admin.DictionaryBesucher()[userVal].Vorname;

                    }
                }
                
            }

            if (BesucheTabelle.Count != 0)
            {
                BesucheGefiltert.ItemsSource = BesucheTabelle;
            }
            else
            {
                BesucheGefiltert.ItemsSource = new List<BesuchManagerTabelle>();

            }
        }

        private void Nachname_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Aktualisieren
           // admin.AlleBesuchDatenVergleichen();
            List<BesuchManagerTabelle> BesucheTabelle = new List<BesuchManagerTabelle>();


            try
            {
                foreach (IBesuch b in admin.ListBesuche())
                {
                    if ((admin.DictionaryBesucher()[b.BesucherID].Nachname).Equals(Nachname.Text))
                    {

                        BesucheTabelle.Add(new BesuchManagerTabelle(b));
                    }

                }

                if (BesucheTabelle.Count != 0)
                {
                    BesucheGefiltert.ItemsSource = BesucheTabelle;
                }
                else
                {
                    BesucheGefiltert.ItemsSource = new List<BesuchManagerTabelle>();

                }
            }
            catch(NullReferenceException)
            {
                Nachname.Text = "Fehlerhafte Eingabe";
            }
        }

        private void RestaurantID_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Aktualisieren
         //   admin.AlleBesuchDatenVergleichen();

            List<BesuchManagerTabelle> BesucheTabelle = new List<BesuchManagerTabelle>();

            foreach (IBesuch b in admin.ListBesuche())
            {
                int userVal;
                if (int.TryParse(RestaurantID.Text, out userVal))
                {
                    if (b.RestaurantID == userVal)
                    {

                        BesucheTabelle.Add(new BesuchManagerTabelle(b));
                    }
                }
                else
                {
                    BesucheGefiltert.ItemsSource = new List<BesuchManagerTabelle>();
                    MessageBox.Show("Bitte tippen sie eine Zahl bei RestaurantID ein!");

                }
            }

            if (BesucheTabelle.Count != 0)
            {
                BesucheGefiltert.ItemsSource = BesucheTabelle;
            }
            else
            {
                //this.Fehler.Text= "Keine Daten zu dieser RestaurantID vorhanden";
                /* List<Seide> li = new List<Seide>();

                 li.Add(new Seide("Keine Daten zu dieser RestaurantID vorhanden"));

                 BesucheGefiltert.ItemsSource = li;
                */
            }
        }
        public class Seide
        {
            public String s;

            public Seide(string s)
            {
                this.s = s;
            }

        }

        private void NurPostiv_Checked(object sender, RoutedEventArgs e)
        {

            //Aktualisieren
           // admin.AlleBesuchDatenVergleichen();

            List<BesuchManagerTabelle> besucheTabPositiv = new List<BesuchManagerTabelle>();

            foreach (IBesuch b in admin.ListBesuche())
            {
                if (admin.DictionaryBesucher()[b.BesucherID].CovidStatus == true)
                {
                    BesuchManagerTabelle bmt = new BesuchManagerTabelle(b);
                    besucheTabPositiv.Add(bmt);
                }

            }

            BesucheGefiltert.ItemsSource = besucheTabPositiv;
        }

        private void NurPostiv_Unchecked(object sender, RoutedEventArgs e)
        {
            BesucheGefiltert.ItemsSource = besucheTab;

        }

        private void NichtBearbeitet_Checked(object sender, RoutedEventArgs e)
        {
            //Aktualisieren
           // admin.AlleBesuchDatenVergleichen();

            //man muss die in der GUI manuell gesetzten Häkchen holen

            List<BesuchManagerTabelle> besucheTab = new List<BesuchManagerTabelle>();


            //normale Tabelle machen jaja is bissle redundant, aber never change a running system
            foreach (IBesuch b in admin.ListBesuche())
            {

                BesuchManagerTabelle bmt = new BesuchManagerTabelle(b);
                if (bmt.Bearbeitet == true)
                {
                    besucheTab.Add(bmt);
                }


            }

            List<BesuchManagerTabelle> BesucheTabNeu = new List<BesuchManagerTabelle>();
            foreach (BesuchManagerTabelle b in besucheTab)
            {
                if (b.Bearbeitet == false)
                {
                    BesucheTabNeu.Add(b);
                }
            }



            BesucheGefiltert.ItemsSource = BesucheTabNeu;
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid BesucheGefiltert  = (DataGrid)sender;
           // CheckedMusicFile ThisMusicfile = (CheckedMusicFile)BesucheGefiltert.SelectedItem;


           // ThisMusicfile.Checked = !ThisMusicfile.Checked;
            BesucheGefiltert.Items.Refresh();
        }
        private void OnChecked(object sender, RoutedEventArgs e)
        {
            
            BesuchManagerTabelle bmt = (BesuchManagerTabelle)e.Source;
            bmt.Bearbeitet = true;
            bmt.CovidStatusBesucher = true;
           
            BesucheGefiltert.Items.Refresh();
        }

        private void OnChecked_Bearbeitet(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("das is ein handler: " + e.Source.ToString() + e.RoutedEvent.ToString());

        }
        private void OnUnChecked_Bearbeitet(object sender, RoutedEventArgs e)
        {
        }



        private void AlarmAussenden_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.log.Info("Alarm aussenden Button geklickt");

            IEmailLogic emailLogic = (IEmailLogic)Application.Current.Properties["IEmailLogic"];
            if (vglTabelle.Count != 0)
            {
                foreach (BesuchManagerTabelle b in vglTabelle)
                {
                    emailLogic.SendEmail(admin.DictionaryBesucher()[admin.DictionaryBesuche()[b.BesuchID].BesucherID].Email, "Sie sind Covid19-Positiv", emailLogic.GetAlertText(b.Restaurant, b.Zeitraum));

                }
                FeedbackAlarmAussenden.Text = "Alarm ausgesandt";
            }
            else
            {
                FeedbackAlarmAussenden.Text = "Tabelle leer! Wählen sie mit Doppelklick einen Besuch zum vergleichen aus";

            }


        }

        private void SelberZeitraum_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void DeleteUser_Click(object sender, RoutedEventArgs e)
        {
            int userVal;
            if (int.TryParse(Del_ID.Text, out userVal))
            {
                admin.DeleteBesucher(userVal);
                MessageBox.Show("Besucher erfolgreich gelöscht!");

            }
            else
            {
                MessageBox.Show("Bitte tippen sie eine Zahl bei UserID ein!");
                }
        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            int userVal;
            if (int.TryParse(UserID_ed.Text, out userVal))
            {
                try
                {
                    if (!Email_ed.Text.Contains('@'))
                    {
                        admin.EditUser(userVal, Email_ed.Text, Nachname_ed.Text, Vorname_ed.Text);
                        MessageBox.Show("Erfolgreich geändert");
                    }
                    else
                    {
                        MessageBox.Show("Falsches Format: Email muss ein @ enthalten!");
                    }
                }
                catch (FormatException ex)
                {
                    MessageBox.Show("Falsches Format: " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Bitte tippen sie eine Zahl bei UserID ein!");
            }
          
        }
    }
}













//Code Mülleimer 


/*  List<IBesuch> besuche = (List<IBesuch>) besAdmn.ListBesuche();

            foreach (IBesuch b in besuche)
                {

                    //Rausfinden wie alt der der Besuch ist
                    DateTime today = DateTime.Today;
                    TimeSpan BesuchAlter = today - b.getDatum();
                    double BesuchAlterInTagen = BesuchAlter.TotalDays;

                    if (BesuchAlterInTagen > 14) //Löschen wenn älter als 14 Tage
                    {
                        besAdmn.DeleteBesuch(b.BesuchID);
                    }                                                                                                                      
                }   */
/* }
 catch (NullReferenceException)
 {
    // Console.WriteLine(e.InnerException.Message);

     Feedback.Text = "FEHLER: NullReferenceException (bes.Admm.ListBesuche() is null (glaub ich))";
 }*/


/*  try
             {*/

//Console.WriteLine(besAdmn.ListBesuche());
// List<IBesuch> besuche =(List<IBesuch>)besAdmn.ListBesuche();
//Length.Text = besuchPers.LoadBesuche().Values.Count.ToString();
/*List<IBesuch> besuche = (List<IBesuch>)besuchPers.LoadBesuche().Values; //wirf NullReferenceException
    int size = besuche.Count();
    Length.Text = size.ToString();*/

/*IDictionary<int, Besuch> besucheDict = besuchPers.LoadBesuche();

List<Besuch> besuche = new List<Besuch>();

foreach (Besuch b in besucheDict.Values)
{
    besuche.Add(b);
}
*/