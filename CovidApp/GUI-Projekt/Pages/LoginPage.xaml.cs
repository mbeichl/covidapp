﻿using CovidApp.AccountVerwaltung.Logic;
using CovidApp.AccountVerwaltung.Util;
using CovidApp.AccountVerwaltung.Util.Impl;
using CovidApp.BesuchVerwaltung;
using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Et.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GUI_Projekt
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Page
    {
        IAccountModule accModule;
        IVerwaltungModule VerwaltungModule;

        public LoginPage()
        {
            InitializeComponent();
            accModule = (IAccountModule)Application.Current.Properties["IAccountModule"];
            VerwaltungModule = (IVerwaltungModule)Application.Current.Properties["IVerwaltungModule"];
        }

        private void Button_Registrieren_Click(object sender, RoutedEventArgs e)
        {
            RegisterPage register = new RegisterPage();
            this.NavigationService.Navigate(register);
        }

        private void Button_Login_Click(object sender, RoutedEventArgs e)
        {
            string PW = PasswordBox_Passwort.Password;
            string UserID = TextBox_UserID.Text;


            if (PW!= "" && PW!=null && UserID != "")
            {             
                int[] response = accModule.validieren(UserID, PW);
                int BesucherID = response[0];
                int lvl = response[1];

                #region Geheime Test logins

                //Lokale Methode damit keine Keys doppelt vorkommen
                void AddUser(object ident)
                {
                    if (Application.Current.Properties["CurrentUser"] != null)
                    {
                        Application.Current.Properties.Remove("CurrentUser");
                    }
                    Application.Current.Properties.Add("CurrentUser", ident);
                }

                if (PasswordBox_Passwort.Password == "genericuser" && TextBox_UserID.Text == "genericuser")
                {
                    IBesucher ident = new Besucher("Hans", "Wurst", "hanswurst@gmail.com");
                    AddUser(ident);
                    IAccountAdmin accAdmin = (IAccountAdmin)Application.Current.Properties["accountAdmin"];
                    IUserAccount genUser = new UserAccount("123", "hanswurst@gmail.com", "Hans", "Wurst");

                    HauptmenuPage hauptmenu = new HauptmenuPage();
                    this.NavigationService.Navigate(hauptmenu);
                    return;
                }
                else if (PasswordBox_Passwort.Password =="besitzer" && TextBox_UserID.Text == "besitzer")
                {
                    IRestaurantBesitzer ident = new RestaurantBesitzer() { name = "Normaler Besitzer", adresse = "Entenhausen", email="generic@mail.com", telefonnummer = "0" };
                    AddUser(ident);
                    BesitzerMenuPage besitzerMenu = new BesitzerMenuPage();
                    this.NavigationService.Navigate(besitzerMenu);
                    return;
                }
                else if (PasswordBox_Passwort.Password == "manager" && TextBox_UserID.Text == "manager")
                {
                    MenuManagerPage managerMenu = new MenuManagerPage();
                    this.NavigationService.Navigate(managerMenu);
                    return;
                }
                else if (PasswordBox_Passwort.Password =="admin" && TextBox_UserID.Text == "admin")
                {
                    MenuAdminPage adminMenu = new MenuAdminPage();
                    this.NavigationService.Navigate(adminMenu);
                    return;
                }
                #endregion
                                
                if (BesucherID > 0)
                {
                    switch (lvl)
                    {
                        case 1:
                            { //Besitzer Login
                                string BesitzerID=accModule.getBesitzerID(UserID);
                                setBesitzer(BesitzerID);
                                BesitzerMenuPage besitzerMenuPage = new BesitzerMenuPage();
                                this.NavigationService.Navigate(besitzerMenuPage);
                                break;
                            }
                        case 2:
                            { // Admin login
                                setBesucher(BesucherID);
                                MenuAdminPage adminPage = new MenuAdminPage();
                                this.NavigationService.Navigate(adminPage);
                                break;
                            }

                        default:
                            { // User login
                                setBesucher(BesucherID);
                                HauptmenuPage hauptmenu = new HauptmenuPage();
                                this.NavigationService.Navigate(hauptmenu);
                                break;
                            }
                    }
                    
                } else
                     MessageBox.Show(String.Format("Email: '{0}' leider nicht mit diesem Passwort gefunden", UserID), "Login Failed", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            } else
                MessageBox.Show(String.Format("Bitte Email und Passwort eingeben", UserID), "Login Failed", MessageBoxButton.OK, MessageBoxImage.Exclamation);      
        }

        #region Focus Event
        /// <summary>
        /// Funktion damit die Sternchen weggehen beim ersten anklicken
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FocusEventPasswort(object sender, RoutedEventArgs e)
        {
            PasswordBox_Passwort.Password = "";
            //nur erstes anklicken löscht
            PasswordBox_Passwort.GotFocus -= FocusEventPasswort;
        }
        private void FocusEventUserID(object sender, RoutedEventArgs e)
        {
            TextBox_UserID.Text = "";
            //nur erstes anklicken löscht
            TextBox_UserID.GotFocus -= FocusEventUserID;
        }


        #endregion

        /*
        private void Button_ManagerMenu_Click(object sender, RoutedEventArgs e)
        {
            MenuManagerPage menuManager = new MenuManagerPage();
            this.NavigationService.Navigate(menuManager);
        }
        private void Button_AdminMenu_Click(object sender, RoutedEventArgs e)
        {
            MenuAdminPage menuAdmin = new MenuAdminPage();
            this.NavigationService.Navigate(menuAdmin);
        } */

        /// <summary>
        /// Funktion die einen Besucher anlegt
        /// Falls man sich mehrmals anmeldet muss CurrentUser geupdatet werden
        /// </summary>
        /// <param name="BesucherID"></param>
        private void setBesucher(int BesucherID)
        {
            IBesucher ident = VerwaltungModule.getBesucher(BesucherID);
            if (Application.Current.Properties["CurrentUser"] != null)
            {
                Application.Current.Properties.Remove("CurrentUser");
            }
            Application.Current.Properties.Add("CurrentUser", ident);
        }
        private void setBesitzer(string BesitzerID)
        {
            IRestaurantBesitzer ident = VerwaltungModule.getRestaurantBesitzer(BesitzerID);
            if (Application.Current.Properties["CurrentUser"] != null)
            {
                Application.Current.Properties.Remove("CurrentUser");
            }
            Application.Current.Properties.Add("CurrentUser", ident);
        }
    }
}
