﻿using CovidApp.AccountVerwaltung.Util;
using CovidApp.BesuchVerwaltung;
using CovidApp.BesuchVerwaltung.Et;
using CovidApp.BesuchVerwaltung.Et.Impl;
using CovidApp.BesuchVerwaltung.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GUI_Projekt
{
    /// <summary>
    /// Interaction logic for MeldenCovidPage.xaml
    /// </summary>
    public partial class MeldenCovidPage : Page
    {
        private IBesucher ident;
        public MeldenCovidPage()
        {
            InitializeComponent();
        }

        private void Fall_Melden_Click(object sender, RoutedEventArgs e)
        {

            IVerwaltungAdmin admin = (IVerwaltungAdmin)Application.Current.Properties["IVerwaltungAdmin"];
            if ((IBesucher)Application.Current.Properties["CurrentUser"] != null)
            {
                

                this.ident = (Besucher)Application.Current.Properties["CurrentUser"];
                if (TestDate.DisplayDate != DateTime.MinValue)
                {

                      
                        if(admin.meldePositiv(ident.BesucherID, TestDate.SelectedDate)) 
                        {

                           
                            MainWindow.log.Info("Positiver Fall gemeldet: " + TestDate.SelectedDate.ToString() + " DateTime.Today: " + DateTime.Today.ToString());
                            MessageBox.Show("Test erfolgreich gemeldet"); 
                        }
                        else
                        {
                            MainWindow.log.Info("Positiver Fall Meldung fehlgeschlagen");
                            MessageBox.Show("FEHLER: Testdatum älter als 14 Tage");

                        }

                   
                }
                else
                {
                    DatumFehlt.Text = "Geben sie bitte ein Datum ein!";
                }
            }
            else {
            }
            
            
            
            
            
            
            
            
            

            
        }
    }
}


















//Ausnahme.Text = TestDate.DisplayDate.ToString();                     

//admin.meldePositiv(ident.BesucherID, TestDate.DisplayDate);
//admin.meldePositiv(ident.BesucherID, TestDate.DisplayDate);

/* using (BesuchContext dbB = new BesuchContext())
 {
     IBesucher besucher1 = dbB.Besucher.Find(34);
     if (besucher1!= null)
{
      dbB.Besucher.  
    Feedback.Text = "Test erfolgreich gemeldet";
}
else
{
    Feedback.Text = "User nicht angemeldet";

}
     dbB.SaveChanges();
 }
 */
