﻿using CovidApp.AccountVerwaltung.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GUI_Projekt.Pages
{
    /// <summary>
    /// Interaktionslogik für PasswortÄndern.xaml
    /// </summary>
    public partial class PasswortÄndern : Page
    {
        private IAccountAdmin admin = (IAccountAdmin)Application.Current.Properties["IAccountAdmin"];
        IAccountModule module = (IAccountModule)Application.Current.Properties["IAccountModule"];

        public PasswortÄndern()
        {
            InitializeComponent();
        }

        private void Button_PasswortÄndern_Click(object sender, RoutedEventArgs e)
        {
            string email = TextBox_Email.Text;
            string altesPW = TextBox_AltesPasswort.Text;
            string neuesPW = TextBox_neuesPasswort.Text;
            string neuesPWwiederholen = TextBox_neuesPasswortWiederholen.Text;

            if(altesPW!="" && neuesPW!="" && neuesPWwiederholen != "" && neuesPW.Length>4 && altesPW != neuesPW && neuesPW.Equals(neuesPWwiederholen)){

                int ID = module.validieren(email, altesPW)[0];
                //wenn ID größer 0 ist, dann ist email in Liste von Accounts enthalten
                if (ID >0) {

                    try
                    {
                        admin.ChangePassword(ID, neuesPW);
                        MessageBox.Show("Passwort erfolgreich geändert");
                        MainWindow.log.Info("Passwort erfolgreich geändert auf: " + neuesPW);
                        HauptmenuPage hauptmenu = new HauptmenuPage();
                        this.NavigationService.Navigate(hauptmenu);
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show("Fehler");
                        MainWindow.log.Info("Passwort ändern: Passwort konnte nicht geändert werden"+ex.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Fehlgeschlagen: Email nicht vorhanden");
                }
            }
            else
            {
                if (neuesPW.Length > 4 == false)
                {
                    MessageBox.Show("Fehlgeschlagen: Beim neuen Passwort ist die Passwortlänge zu kurz: mind. vierstellig");

                }
                else if (altesPW == neuesPW)
                {
                    MessageBox.Show("Fehlgeschlagen: Altes und neues Passwort dürfen nicht übereinstimmen");

                }
                else if (neuesPW.Equals(neuesPWwiederholen) == false)
                {
                    MessageBox.Show("Fehlgeschlagen: Neues Passwort wiederholen stimmt nicht überein");
                }
                else
                {
                    MessageBox.Show("Fehlgeschlagen");
                }
            }
        }
    }
}
