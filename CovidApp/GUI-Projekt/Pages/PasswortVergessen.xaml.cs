﻿using CovidApp.AccountVerwaltung.Logic;
using CovidApp.Email;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GUI_Projekt
{
    /// <summary>
    /// Interaction logic for PasswortVergessenPage.xaml
    /// </summary>
    public partial class PasswortVergessenPage : Page
    {
        private IAccountAdmin admin; 
        private IAccountModule module;
        private IEmailLogic emailLogic = (IEmailLogic)Application.Current.Properties["IEmailLogic"];

        public PasswortVergessenPage()
        {
            InitializeComponent();
            admin = (IAccountAdmin) Application.Current.Properties["IAccountAdmin"];
            module = (IAccountModule)Application.Current.Properties["IAccountModule"];

        }
    

        private void Button_Beantragen_Click(object sender, RoutedEventArgs e)
        {

            string email = TextBox_Email.Text;

            if (email!="" && email!=null) {

                if (admin.CheckDuplicate(email) == true)
                {
                    string neuesPasswort = admin.GetRandomPassword();
                    int userID = module.getUserID(email);
                    try
                    {
                        emailLogic.SendEmail(email, "Neues Passwort beantragt", emailLogic.GetPasswordText(neuesPasswort));

                        admin.ChangePassword(userID, neuesPasswort);
                        MessageBox.Show("Neues Passwort wurde verschickt!");
                        LoginPage login = new LoginPage();
                        this.NavigationService.Navigate(login);
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show("Fehler");
                        MainWindow.log.Info("Passwort beantragen Click: Passwort konnte nicht beantragt werden " + ex.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Email nicht vorhanden");
                }
            }
            else
            {
                MessageBox.Show("Fehler: Passwort konnte nicht beantragt werden");
            }
        }
        

    }
}
