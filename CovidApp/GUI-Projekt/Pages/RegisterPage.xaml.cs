﻿using CovidApp.AccountVerwaltung.Logic;
using CovidApp.AccountVerwaltung.Util;
using CovidApp.AccountVerwaltung.Util.Impl;
using CovidApp.BesuchVerwaltung;
using CovidApp.BesuchVerwaltung.Et;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GUI_Projekt
{
    /// <summary>
    /// Interaction logic for Register.xaml
    /// </summary>
    public partial class RegisterPage : Page, INotifyPropertyChanged
    {
        IAccountAdmin accAdmin;
        IAccountModule accModule;
        IVerwaltungAdmin besuchAdmin;

        #region INotifyPropertyChanged
        //damit der Popup text sich ändert
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private string _AlertText;

        public string AlertText
        {
            get { return _AlertText; }
            set
            {
                if (value != _AlertText)
                {
                    _AlertText = value;
                    OnPropertyChanged("AlertText");
                }
            }
        }
        #endregion

        public RegisterPage()
        {
            InitializeComponent();
            AlertEingabe.DataContext = this;
            accAdmin = (IAccountAdmin)Application.Current.Properties["IAccountAdmin"];
            accModule = (IAccountModule)Application.Current.Properties["IAccountModule"];
            besuchAdmin = (IVerwaltungAdmin)Application.Current.Properties["IVerwaltungAdmin"];
        }

        private void Button_Registrieren_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.log.Info("Registrieren: Button Click");
            if(TextBox_Email.Text=="" || TextBox_Nachname.Text == "" || TextBox_Passwort1.Text == "" || TextBox_Passwort2.Text == "" || TextBox_Vorname.Text == "")
            {
                PopupMessage("Bitte Daten eingeben", false);                
                //MessageBox.Show("Bitte Daten eingeben", "Eingabe ungültig", MessageBoxButton.OK, MessageBoxImage.Information);
            } else if(TextBox_Passwort1.Text.Length<3){
                PopupMessage("Passwort zu kurz!", false);
            }
            else if (TextBox_Passwort1.Text != TextBox_Passwort2.Text)
            {
                MainWindow.log.Info("Registrieren: Passwörter stimmen nicht überein");
                PopupMessage("Passwörter stimmen nicht überein", false);
            } else if (!isEmail(TextBox_Email.Text))
            {
                PopupMessage("E-Mail Format falsch", false);
            }
            else
            {
                try
                {
                    Register();

                    LoginPage login = new LoginPage();
                    this.NavigationService.Navigate(login);
                }
                catch (Exception ex)
                {
                    MainWindow.log.Info("Exception bei Register: " + ex.Message);
                    PopupMessage(ex.Message, false);
                }

            }
           
        }
        private bool isEmail(string s)
        {
            try
            {
                var a = new System.Net.Mail.MailAddress(s);
                return a.Address == s;
            }
            catch
            {
                return false;
            }
        }


        private void Register()
        {           
                IUserAccount u = accAdmin.CreateUserAccount(TextBox_Passwort1.Text, TextBox_Email.Text, TextBox_Vorname.Text, TextBox_Nachname.Text);
                MainWindow.log.Info("UserAccount angelegt mit ID" + u.userID);

                IBesucher b = besuchAdmin.AddBesucher(TextBox_Vorname.Text, TextBox_Nachname.Text, TextBox_Email.Text);
                MainWindow.log.Info("Besucher angelegt mit ID" + b.BesucherID);

                accAdmin.AddBesucherID(u.userID, b.BesucherID);           
        }
        private void PopupMessage(String e, bool b)
        {
            if (b)
            {
                AlertEingabe.IsOpen = true;
                AlertEingabe.Visibility = System.Windows.Visibility.Visible;
                AlertText = "Registrierung erfolgreich " + e;
            }
            else
            {
                AlertBlock.Text = e;
                AlertEingabe.IsOpen = true;
                AlertEingabe.Visibility = System.Windows.Visibility.Visible;                             
            }
            //timer für Popup
            DispatcherTimer time = new DispatcherTimer();
            time.Interval = TimeSpan.FromSeconds(2);
            time.Start();
            time.Tick += delegate
            {
                AlertEingabe.IsOpen = false;
                time.Stop();
            };
        }
    }
}
